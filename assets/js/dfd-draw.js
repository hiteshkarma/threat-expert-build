
function clearElement(i) {
    if (i.hasChildNodes())
        while (i.childNodes.length >= 1) i.removeChild(i.firstChild)
    return i
}
function createElement(type, att, parent, inn) {
    var e = document.createElement(type)

    if (inn == undefined) {
        inn = ''
    }
    if (inn != '') e.innerHTML = inn
    for (var i = 0; i < att.length; i++)
        e.setAttribute(att[i][0], att[i][1])
    parent.appendChild(e)
    return e
}
function S4() {
    return (((1 + Math.random()) * 0x10000) | 0)
        .toString(16)
        .substring(1)
}
function guid() {
    return (
        S4() +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        S4() +
        S4()
    )
}
function elementOffsets(element, s) {
    var top = 0,
        left = 0
    var el = element
    do {
        top += el.offsetTop || 0
        left += el.offsetLeft || 0

        el = el.offsetParent
    } while (el)
    if (s != undefined) {
        do {
            if (element == eGet()) {
            } else if (
                (element.scrollTop || element.scrollLeft) &&
                String(element.nodeName).toLocaleUpperCase() != 'HTML'
            ) {
                top -= element.scrollTop
                left -= element.scrollLeft
            }
            element = element.parentNode
        } while (element)
    }
    return [left, top]
}
function insertAfter(referenceNode, newNode) {
    if (referenceNode == null) {
        return
    }
    referenceNode.parentNode.insertBefore(
        newNode,
        referenceNode.nextSibling
    )
}
function insertBefore(referenceNode, newNode) {
    if (referenceNode == null) {
        return
    }
    referenceNode.parentNode.insertBefore(newNode, referenceNode)
}
function DFD(ele) {
    var self = this

    clearElement(ele)

    var canvasEle = createElement('div', [['class', 'dfd-canvas']], ele)
    self.width = canvasEle.clientWidth
    self.height = canvasEle.clientHeight
    self.cx = self.width / 2
    self.cy = self.height / 2
    var canvas = Snap(self.width, self.height)
    canvasEle.appendChild(canvas.node)
    self.canvas = canvas
    self.previewLine = canvas.path()
    self.linksPaper = canvas.g()
    self.linksTextPaper = canvas.g()
    self.elementsPaper = canvas.g()
    var markerPath = canvas
        .path('M 2 59 L 293 148 L 1 243 L 121 151 Z')
        .attr({ class: 'mid-marker' })
    self.midMarker = markerPath
        .marker(0, 0, 4000, 4000, 250, 150)
        .attr({
            markerUnits: 'strokeWidth',
            markerWidth: 150,
            markerHeight: 150,
            orient: 'auto'
        })
    self.previewLine.attr({
        class: 'dfd-preview-line',
        markerMid: self.midMarker
    })
    var elementsList = createElement(
        'div',
        [['class', 'dfd-elements']],
        ele
    )
    var elements = [
        { name: 'rectangle', shape: 'rectangle' },
        { name: 'circle', shape: 'circle' },
        { name: 'circle-double', shape: 'circle-double' },
        {
            name: 'rectangle-dotted',
            shape: 'rectangle-dotted',
            empty: true,
            class: 'dfd-rectangle-dotted'
        }
    ]
    self.selectInfoEle = createElement(
        'div',
        [['class', 'dfd-select-info']],
        ele
    )
    self.initSelectInfoEle()
    for (var i = 0; i < elements.length; i++) {
        var element = createElement(
            'div',
            [['class', 'dfd-element']],
            elementsList
        )
        var paper = Snap(40, 40)
        element.appendChild(paper.node)
        self.addNode(elements[i], { width: 30, height: 30, paper: paper })
        element.addEventListener(
            'click',
            (function (d) {
                return function () {
                    self.addNode(d)
                }
            })(elements[i])
        )
    }
    this.nodes = {}
    this.links = []
    var moveObj = {}
    window.addEventListener('mousemove', function (event) {
        if (self.moveObj.mode !== 'move') {
            return
        }
        if (self.nodes[self.moveObj.object.id] == undefined) {
            return
        }
        var diffX = self.moveObj.clientX - event.clientX
        var diffY = self.moveObj.clientY - event.clientY
        var o = self.moveObj.object
        var g = o.g
        var dim = o.dim
        var dim_ = self.moveObj.object_.dim
        dim.x = dim_.x - diffX
        dim.y = dim_.y - diffY
        g.attr({
            transform: 'translate(' + dim.x + ' ' + dim.y + ')'
        })

        self.updateLinks()
    })
    window.addEventListener('mousemove', function (event) {
        if (self.moveObj.mode !== 'link') {
            return
        }
        var o = self.moveObj.object
        var dim = o.dim
        var ofs = elementOffsets(self.canvas.node.parentNode)
        var x2 = event.clientX - ofs[0]
        var y2 = event.clientY - ofs[1]
        var x1 = dim.x + dim.width / 2
        var y1 = dim.y + dim.height / 2
        self.previewLine.attr({ d: self.linkPath(x1, y1, x2, y2) })
    })
    window.addEventListener('mousemove', function (event) {
        if (
            [
                'resize-nw',
                'resize-ne',
                'resize-sw',
                'resize-se',
                'resize-s',
                'resize-n',
                'resize-w',
                'resize-e'
            ].indexOf(self.moveObj.mode) < 0
        ) {
            return
        }
        var diffX = self.moveObj.clientX - event.clientX
        var diffY = self.moveObj.clientY - event.clientY
        var o = self.moveObj.object
        var dim_ = self.moveObj.object_.dim
        var g = o.g
        var dim = o.dim
        var ratio = dim_.width / dim_.height
        if (['resize-nw'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio =
                    (dim_.width + diffX) / (dim_.height + diffY)
                if (ratio < newRatio) {
                    diffX = ratio * (dim_.height + diffY) - dim_.width
                } else {
                    diffY = (dim_.width + diffX) / ratio - dim_.height
                }
            }
            dim.x = dim_.x - diffX
            dim.y = dim_.y - diffY
            dim.width = dim_.width + diffX
            dim.height = dim_.height + diffY
        }
        if (['resize-ne'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio =
                    (dim_.width - diffX) / (dim_.height + diffY)
                if (ratio < newRatio) {
                    diffX =
                        -1 *
                        (ratio * (dim_.height + diffY) - dim_.width)
                } else {
                    diffY = (dim_.width - diffX) / ratio - dim_.height
                }
            }
            dim.y = dim_.y - diffY
            dim.width = dim_.width - diffX
            dim.height = dim_.height + diffY
        }
        if (['resize-sw'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio =
                    (dim_.width + diffX) / (dim_.height - diffY)
                if (ratio < newRatio) {
                    diffX = ratio * (dim_.height - diffY) - dim_.width
                } else {
                    diffY =
                        -1 *
                        ((dim_.width + diffX) / ratio - dim_.height)
                }
            }
            dim.x = dim_.x - diffX
            dim.width = dim_.width + diffX
            dim.height = dim_.height - diffY
        }
        if (['resize-se'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio =
                    (dim_.width - diffX) / (dim_.height - diffY)
                if (ratio < newRatio) {
                    diffX =
                        -1 *
                        (ratio * (dim_.height - diffY) - dim_.width)
                } else {
                    diffY =
                        -1 *
                        ((dim_.width - diffX) / ratio - dim_.height)
                }
            }
            dim.width = dim_.width - diffX
            dim.height = dim_.height - diffY
        }
        if (['resize-n'].indexOf(self.moveObj.mode) >= 0) {
            dim.y = dim_.y - diffY
            dim.height = dim_.height + diffY
        }
        if (['resize-w'].indexOf(self.moveObj.mode) >= 0) {
            dim.x = dim_.x - diffX
            dim.width = dim_.width + diffX
        }
        if (['resize-e'].indexOf(self.moveObj.mode) >= 0) {
            dim.width = dim_.width - diffX
        }
        if (['resize-s'].indexOf(self.moveObj.mode) >= 0) {
            dim.height = dim_.height - diffY
        }
        g.attr({
            transform: 'translate(' + dim.x + ' ' + dim.y + ')'
        })
        self.updateResizeObj(o)
        o.draw(o)
        self.updateLinks()
    })

    window.addEventListener('mouseup', function () {
        self.moveObj = {}
        self.previewLine.attr({ d: '' })
    })
    self.moveObj = moveObj
    window.addEventListener('click', function (event) {
        self.clearSelect()
    })
}
DFD.prototype.initSelectInfoEle = function () {
    var self = this
    var parent = self.selectInfoEle
    parent.addEventListener('click', function (event) {
        event.stopPropagation()
    })
    var iconsRow = createElement(
        'div',
        [['class', 'dfd-info-name']],
        parent,
        'Name'
    )
    var iconsRow = createElement(
        'div',
        [['class', 'dfd-info-icons']],
        parent
    )

    createElement(
        'button',
        [['class', 'dfd-info-icon']],
        iconsRow,
        'Bring Front'
    ).addEventListener('click', function (event) {
        var node = self.selectedObject.g.node
        event.stopPropagation()
        insertAfter(node.parentNode.lastChild, node)
    })
    createElement(
        'button',
        [['class', 'dfd-info-icon']],
        iconsRow,
        'Bring Forward'
    ).addEventListener('click', function (event) {
        var node = self.selectedObject.g.node
        event.stopPropagation()
        insertAfter(node.nextSibling, node)
    })
    createElement(
        'button',
        [['class', 'dfd-info-icon']],
        iconsRow,
        'Send Backward'
    ).addEventListener('click', function (event) {
        var node = self.selectedObject.g.node
        event.stopPropagation()
        insertBefore(node.previousSibling, node)
    })
    createElement(
        'button',
        [['class', 'dfd-info-icon']],
        iconsRow,
        'Send Back'
    ).addEventListener('click', function (event) {
        var node = self.selectedObject.g.node
        event.stopPropagation()
        insertBefore(node.parentNode.firstChild, node)
    })

    createElement(
        'button',
        [['class', 'dfd-remove-icon']],
        parent,
        'Remove'
    ).addEventListener('click', function (event) {
        event.stopPropagation()
        var links = []
        for (var i = 0; i < self.links.length; i++) {
            var link = self.links[i]
            if (
                link.source.id != self.selectedObject.id &&
                link.target.id != self.selectedObject.id
            ) {
                links.push(link)
            } else {
                var path = link.path.node
                var text = link.text.node
                path.parentNode.removeChild(path)
                text.parentNode.removeChild(text)
            }
        }
        self.links = links
        var node = self.selectedObject.g.node
        node.parentNode.removeChild(node)
        delete self.nodes[self.selectedObject.id]
    })
}

DFD.prototype.onSelect = function (obj) {
    var self = this
    self.selectedObject = obj
    obj.g.node.classList.add('dfd-shape-selected')
    self.selectInfoEle.classList.add('dfd-select-info-visible')
    self.selectInfoEle.querySelector('.dfd-info-name').innerHTML =
        obj.element.name
}
DFD.prototype.clearSelect = function () {
    var self = this
    self.selectInfoEle.classList.remove('dfd-select-info-visible')
    if (self.selectedObject == undefined) {
        return
    }
    var obj = self.selectedObject
    obj.g.node.classList.remove('dfd-shape-selected')
    delete self.selectedObject
}
DFD.prototype.linkPath = function (x1, y1, x2, y2) {
    var self = this
    var midX = (x1 + x2) / 2
    var midY = (y1 + y2) / 2
    // return (
    //     'M' +
    //     x1 +
    //     ' ' +
    //     y1 +
    //     ' L' +
    //     midX +
    //     ' ' +
    //     y1 +
    //     // ' L' +
    //     // x2 +
    //     // ' ' +
    //     // y1 +
    //     ' L' +
    //     x2 +
    //     ' ' +
    //     midY +
    //     ' L' +
    //     x2 +
    //     ' ' +
    //     y2
    // )
    return self.drawPath([[x1, y1], [midX, y1], [x2, midY], [x2, y2]])
}
DFD.prototype.createLink = function (source, target) {
    var self = this
    console.log(source)
    console.log(target)
    if (source.id === target.id) {
        return
    }
    if (source.element.empty) {
        return
    }
    if (target.element.empty) {
        return
    }

    for (var i = 0; i < self.links.length; i++) {
        var l = self.links[i]
        if (l.source.id == source.id && l.target.id == target.id) {
            return
        }
    }
    var linkObj = {}
    linkObj.source = source
    linkObj.target = target
    var x1 = source.dim.x + source.dim.width / 2
    var x2 = target.dim.x + target.dim.width / 2
    var y1 = source.dim.y + source.dim.height / 2
    var y2 = target.dim.y + target.dim.height / 2
    linkObj.path = self.linksPaper.path()
    linkObj.path.attr({
        class: 'dfd-preview-line',
        d: self.linkPath(x1, y1, x2, y2),
        markerMid: self.midMarker
    })
    var midX = (x1 + x2) / 2
    var midY = (y1 + y2) / 2
    linkObj.text = self.tSpan(
        self.linksTextPaper,
        (midX + x2) / 2,
        (midY + y1) / 2,
        'Message'
    )
    self.links.push(linkObj)
}
DFD.prototype.updateLinks = function () {
    var self = this
    for (var i = 0; i < self.links.length; i++) {
        var l = self.links[i]
        var source = l.source
        var target = l.target
        var x1 = source.dim.x + source.dim.width / 2
        var x2 = target.dim.x + target.dim.width / 2
        var y1 = source.dim.y + source.dim.height / 2
        var y2 = target.dim.y + target.dim.height / 2
        l.path.attr({
            d: self.linkPath(x1, y1, x2, y2)
        })
        var midX = (x1 + x2) / 2
        var midY = (y1 + y2) / 2
        l.text.attr({
            x: (midX + x2) / 2,
            y: (midY + y1) / 2
        })
    }
}
DFD.prototype.addNode = function (element, paperObj) {
    var self = this
    var width = 100
    var height = 100
    if (paperObj) {
        width = paperObj.width
        height = paperObj.height
    }
    var dim = {
        x: self.cx - width / 2,
        y: self.cy - height / 2,
        width: width,
        height: height
    }
    var id = guid()
    var g = undefined
    if (paperObj) {
        dim.x = 5
        dim.y = 5
        g = paperObj.paper.g()
    } else {
        g = self.elementsPaper.g()
    }

    g.attr({ transform: 'translate(' + dim.x + ' ' + dim.y + ')' })

    var obj = {}
    obj.id = id
    obj.g = g
    obj.dim = dim
    obj.element = element
    if (element.shape == 'rectangle-dotted') {
        obj.draw = function (o) {
            if (o.shape == undefined) {
                o.shape = o.g.rect(0, 0, o.dim.width, o.dim.height)
            } else {
                o.shape.attr({
                    x: 0,
                    y: 0,
                    width: o.dim.width,
                    height: o.dim.height
                })
            }
        }
    }
    if (element.shape == 'circle-double') {
        obj.draw = function (o) {
            var inner = -2
            if (o.shape == undefined) {
                o.shape = [
                    o.g.ellipse(
                        o.dim.width / 2,
                        o.dim.height / 2,
                        o.dim.width / 2,
                        o.dim.height / 2
                    ),
                    o.g.ellipse(
                        o.dim.width / 2,
                        o.dim.height / 2,
                        o.dim.width / 2 + inner * 2,
                        o.dim.height / 2 + inner * 2
                    )
                ]
            } else {
                o.shape[0].attr({
                    cx: o.dim.width / 2,
                    cy: o.dim.height / 2,
                    rx: o.dim.width / 2,
                    ry: o.dim.height / 2
                })
                o.shape[1].attr({
                    cx: o.dim.width / 2,
                    cy: o.dim.height / 2,
                    rx: o.dim.width / 2 + inner * 2,
                    ry: o.dim.height / 2 + inner * 2
                })
            }
        }
    }
    if (element.shape == 'circle') {
        obj.draw = function (o) {
            if (o.shape == undefined) {
                o.shape = o.g.ellipse(
                    o.dim.width / 2,
                    o.dim.height / 2,
                    o.dim.width / 2,
                    o.dim.height / 2
                )
            } else {
                o.shape.attr({
                    cx: o.dim.width / 2,
                    cy: o.dim.height / 2,
                    rx: o.dim.width / 2,
                    ry: o.dim.height / 2
                })
            }
        }
    }
    if (element.shape == 'rectangle') {
        obj.draw = function (o) {
            if (o.shape == undefined) {
                o.shape = o.g.rect(0, 0, o.dim.width, o.dim.height)
            } else {
                o.shape.attr({
                    x: 0,
                    y: 0,
                    width: o.dim.width,
                    height: o.dim.height
                })
            }
        }
    }
    if (obj.draw) {
        obj.draw(obj)

        var shapeNodes = []
        var cls = 'dfd-shape'
        if (obj.element.class) {
            cls = obj.element.class
        }
        if (obj.shape.node) {
            obj.shape.attr({ class: cls })
            shapeNodes = [obj.shape]
        } else {
            for (var i = 0; i < obj.shape.length; i++) {
                obj.shape[i].attr({ class: cls })
            }
            shapeNodes = obj.shape
        }
    }
    if (paperObj) {
        return
    }

    for (var i = 0; i < shapeNodes.length; i++) {
        var node = shapeNodes[i].node
        node.addEventListener('click', function (event) {
            self.clearSelect()

            self.onSelect(obj)

            event.stopPropagation()
        })
        node.addEventListener('mousedown', function (event) {
            if (!event.ctrlKey) {
                self.moveObj = {
                    object: obj,
                    object_: JSON.parse(JSON.stringify(obj)),
                    event: event,
                    clientX: event.clientX,
                    clientY: event.clientY,
                    mode: 'move'
                }
            } else {
                if (!obj.element.empty) {
                    self.moveObj = {
                        object: obj,
                        mode: 'link',
                        event: event,
                        clientX: event.clientX,
                        clientY: event.clientY
                    }
                }
            }
        })
        node.addEventListener('mouseup', function (event) {
            if (event.ctrlKey) {
                self.createLink(self.moveObj.object, obj)
            }
        })
    }

    var resizePoints = {}
    var pointRadius = 5
    resizePoints.topLeft = g.circle(0, 0, pointRadius)
    resizePoints.topLeft.attr({
        class: 'dfd-resize-points dfd-resize-top-left'
    })
    resizePoints.topRight = g.circle(dim.width, 0, pointRadius)
    resizePoints.topRight.attr({
        class: 'dfd-resize-points dfd-resize-top-right'
    })
    resizePoints.bottomLeft = g.circle(0, dim.height, pointRadius)
    resizePoints.bottomLeft.attr({
        class: 'dfd-resize-points dfd-resize-bottom-left'
    })
    resizePoints.bottomRight = g.circle(
        dim.width,
        dim.height,
        pointRadius
    )
    resizePoints.bottomRight.attr({
        class: 'dfd-resize-points dfd-resize-bottom-right'
    })

    resizePoints.left = g.circle(0, dim.height / 2, pointRadius)
    resizePoints.left.attr({
        class: 'dfd-resize-points dfd-resize-left'
    })

    resizePoints.top = g.circle(dim.width / 2, 0, pointRadius)
    resizePoints.top.attr({
        class: 'dfd-resize-points dfd-resize-top'
    })

    resizePoints.right = g.circle(
        dim.width,
        dim.height / 2,
        pointRadius
    )
    resizePoints.right.attr({
        class: 'dfd-resize-points dfd-resize-right'
    })
    resizePoints.bottom = g.circle(
        dim.width / 2,
        dim.height,
        pointRadius
    )
    resizePoints.bottom.attr({
        class: 'dfd-resize-points dfd-resize-bottom'
    })
    for (var p in resizePoints) {
        var pEle = resizePoints[p].node
        pEle.addEventListener('click', function () {
            event.stopPropagation()
        })
    }
    function updateMoveObject(mode, event) {
        self.moveObj = {
            object: obj,
            object_: JSON.parse(JSON.stringify(obj)),
            mode: mode,
            event: event,
            clientX: event.clientX,
            clientY: event.clientY
        }
        event.stopPropagation()
    }
    resizePoints.topLeft.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-nw', event)
    })
    resizePoints.topRight.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-ne', event)
    })
    resizePoints.bottomLeft.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-sw', event)
    })
    resizePoints.bottomRight.node.addEventListener(
        'mousedown',
        function (event) {
            updateMoveObject('resize-se', event)
        }
    )
    resizePoints.left.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-w', event)
    })
    resizePoints.top.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-n', event)
    })
    resizePoints.right.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-e', event)
    })
    resizePoints.bottom.node.addEventListener('mousedown', function (
        event
    ) {
        updateMoveObject('resize-s', event)
    })
    obj.resizePoints = resizePoints
    self.nodes[obj.id] = obj
}
DFD.prototype.updateResizeObj = function (obj) {
    var resizePoints = obj.resizePoints
    var dim = obj.dim
    resizePoints.topRight.attr({ cx: dim.width })
    resizePoints.bottomLeft.attr({ cy: dim.height })
    resizePoints.bottomRight.attr({ cy: dim.height, cx: dim.width })

    resizePoints.top.attr({ cx: dim.width / 2 })
    resizePoints.right.attr({ cy: dim.height / 2, cx: dim.width })
    resizePoints.left.attr({ cy: dim.height / 2 })
    resizePoints.bottom.attr({ cx: dim.width / 2, cy: dim.height })
}
DFD.prototype.tSpan = function (paper, x, y, text) {
    return paper.text(x, y, text)
}
DFD.prototype.drawPath = function (points) {
    var opposedLine = function (pointA, pointB) {
        var lengthX = pointB[0] - pointA[0]
        var lengthY = pointB[1] - pointA[1]
        return {
            length: Math.sqrt(
                Math.pow(lengthX, 2) + Math.pow(lengthY, 2)
            ),
            angle: Math.atan2(lengthY, lengthX)
        }
    }
    var controlPoint = function (current, previous, next, reverse) {
        var p = previous || current
        var n = next || current
        var smoothing = 0.2
        var o = opposedLine(p, n)
        var angle = o.angle + (reverse ? Math.PI : 0)
        var length = o.length * smoothing
        var x = current[0] + Math.cos(angle) * length
        var y = current[1] + Math.sin(angle) * length
        return [x, y]
    }
    var bezierCommand = function (point, i, a) {
        var [cpsX, cpsY] = controlPoint(a[i - 1], a[i - 2], point)
        var [cpeX, cpeY] = controlPoint(point, a[i - 1], a[i + 1], true)
        return `C ${cpsX},${cpsY} ${cpeX},${cpeY} ${point[0]},${
            point[1]
            }`
    }
    var d = points.reduce(function (acc, point, i, a) {
        return i === 0
            ? `M ${point[0]},${point[1]}`
            : `${acc} ${bezierCommand(point, i, a)}`
    }, '')
    return d
}

setTimeout(() => {
    var C = new DFD(document.getElementById('dfd-canvas'))
}, 5000);