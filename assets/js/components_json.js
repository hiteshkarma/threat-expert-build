var COMPONENTS_DATA = [
    {
        Actor: {
            isAdmin: null
        },
        icon: './../assets/images/circle.png'
    },
    {
        Server: {
            usesLatestTLSversion: true,
            implementsAuthenticationScheme: true,
            authorizesSource: true,
            isHardened: true,
            checksInputBounds: true,
            hasAccessControl: true,
            disablesDTD: true,
            HTTPSenabled: null,
            usesCache: false,
            usesXMLParser: true,
            authenticatesSource: true,
            RSAorAESEncryptionAlgorithmUsed: null,
            SMTPenabled: null,
            IMAPenabled: null,
            SOAPenabled: null
        },
        icon: './../assets/images/server.png'
    },
    {
        Lambda: {
            authorizesSource: true,
            handlesResourceConsumption: true,
            sanitizesInput: true,
            checksInputBounds: true,
            hasAccessControl: null,
            encodesOutput: null,
            usesEnvironmentVariables: false,
            validatesInput: true,
            ProductionEnvironment: false,
            authenticatesSource: null
        },
        icon: './../assets/images/aws-lambda.png'
    },
    {
        ExternalEntity: {
            hasPhysicalAccess: false
        },
        icon: './../assets/images/rectangle-round.svg'
    },
    {
        Datastore: {
            handlesInterruptions: null,
            isShared: false,
            isEncrypted: null,
            handlesResourceConsumption: true,
            storesLogData: null,
            hasAccessControl: true,
            storesSensitiveData: null,
            implementsPOLP: true,
            validatesInput: true,
            storesPII: null,
            authenticatesSource: null,
            RSAorAESEncryptionAlgorithmUsed: true
        },
        icon: './../assets/images/database.png'
    },
    {
        Process: {
            allowsClientSideScripting: false,
            usesParameterizedInput: true,
            tracksExecutionFlow: null,
            implementsAuthenticationScheme: true,
            authorizesSource: true,
            implementsNonce: true,
            definesConnectionTimeout: true,
            handlesResourceConsumption: true,
            implementsCSRFToken: true,
            sanitizesInput: true,
            checksInputBounds: true,
            usesStrongSessionIdentifiers: null,
            hasAccessControl: true,
            isResilient: true,
            encodesOutput: true,
            usesEnvironmentVariables: false,
            implementsPOLP: true,
            validatesInput: true,
            disablesiFrames: true,
            implementsCommunicationProtocol: null,
            usesSecureFunctions: true,
            encryptsSessionData: true,
            implementsAPI: false,
            ProductionEnvironment: false,
            verifySessionIdentifiers: true,
            authenticatesSource: true,
            usesMFA: true,
            JSONDataFormatUsed: false
        },
        icon: './../assets/images/rectangle.svg'
    },
    {
        Dataflow: {
            usesLatestTLSversion: true,
            isEncrypted: true,
            authorizesSource: true,
            implementsNonce: null,
            usesSessionTokens: false,
            usesVPN: true,
            HTTPSenabled: true,
            protocol: null,
            XMLDataFormatUsed: false
        },
        icon: './../assets/images/pentagon.svg'
    },
    {
        API: {
            usesParameterizedInput: true,
            loggingenabled: true,
            authorizesSource: true,
            usesSessionTokens: false,
            handlesResourceConsumption: true,
            implementsCSRFToken: true,
            sanitizesInput: true,
            checksInputBounds: true,
            hasAccessControl: true,
            encodesOutput: true,
            isHTTPMethodsEnforced: null,
            HTTPSenabled: true,
            validatesInput: true,
            validatesHeaders: null,
            ProductionEnvironment: false,
            authenticatesSource: true
        },
        icon: './../assets/images/diamond.svg'
    },
    {
        SetOfProcesses: {
            allowsClientSideScripting: false,
            usesParameterizedInput: true,
            tracksExecutionFlow: null,
            implementsAuthenticationScheme: true,
            authorizesSource: true,
            implementsNonce: true,
            definesConnectionTimeout: true,
            handlesResourceConsumption: true,
            implementsCSRFToken: true,
            sanitizesInput: true,
            checksInputBounds: true,
            usesStrongSessionIdentifiers: null,
            hasAccessControl: true,
            isResilient: true,
            encodesOutput: true,
            usesEnvironmentVariables: false,
            implementsPOLP: true,
            validatesInput: true,
            disablesiFrames: true,
            implementsCommunicationProtocol: null,
            usesSecureFunctions: true,
            encryptsSessionData: true,
            implementsAPI: false,
            ProductionEnvironment: false,
            verifySessionIdentifiers: true,
            authenticatesSource: true,
            usesMFA: true,
            JSONDataFormatUsed: false
        },
        icon: './../assets/images/rectangle.svg'
    },
    {
        test: {
            loggingenabled: true,
            test_property: true
        },
        icon: './../assets/images/rectangle.svg'
    }
]
