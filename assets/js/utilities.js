function clearElement(i) {
    if (i.hasChildNodes())
        while (i.childNodes.length >= 1) i.removeChild(i.firstChild)
    return i
}
function createElement(type, att, parent, inn) {
    var e = document.createElement(type)

    if (inn !== undefined) {
        e.innerHTML = inn
    }
    for (var i in att) {
        e.setAttribute(i, att[i])
    }
    parent.appendChild(e)
    return e
}
function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
}
function forList(obj, fun) {
    var t = Object.prototype.toString.call(obj)
    var c = 0
    if (t == '[object Array]' || t == '[object NodeList]') {
        for (var i = 0; i < obj.length; i++) {
            fun(obj[i], i)
        }
    } else {
        for (var i in obj) {
            fun(obj[i], i, c)
            c++
        }
    }
}
function randomID() {
    return (
        S4() +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        '-' +
        S4() +
        S4() +
        S4()
    )
}
function elementOffsets(element, s) {
    var top = 0,
        left = 0
    var el = element
    do {
        top += el.offsetTop || 0
        left += el.offsetLeft || 0

        el = el.offsetParent
    } while (el)
    if (s != undefined) {
        do {
            if (element == eGet()) {
            } else if (
                (element.scrollTop || element.scrollLeft) &&
                String(element.nodeName).toLocaleUpperCase() != 'HTML'
            ) {
                top -= element.scrollTop
                left -= element.scrollLeft
            }
            element = element.parentNode
        } while (element)
    }
    return [left, top]
}
function insertAfter(referenceNode, newNode) {
    if (referenceNode == null) {
        return
    }
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling)
}
function insertBefore(referenceNode, newNode) {
    if (referenceNode == null) {
        return
    }
    referenceNode.parentNode.insertBefore(newNode, referenceNode)
}
function loadShape(path, onLoad) {
    function loadSVG(a) {
        var loadedSvg = document.createElement('svg')
        loadedSvg.innerHTML = a
        var paths = loadedSvg.querySelectorAll('path')
        var viewBox = loadedSvg.querySelector('svg').getAttribute('viewBox')
        var pathAttributes = []
        forList(paths, function(d) {
            var attributeList = ['transform', 'd']
            var attributes = []
            var style = d.getAttribute('style').split(';')
            var styleMap = {}
            forList(style, function(s) {
                var s_ = s.split(':')
                styleMap[s_[0]] = s_[1]
            })

            forList(attributeList, function(a) {
                if (d.getAttribute(a) != null) {
                    attributes.push([a, d.getAttribute(a)])
                }
            })
            pathAttributes.push(attributes)
        })
        onLoad({pathAttributes: pathAttributes, viewBox: viewBox})
    }
    var xhr = new XMLHttpRequest()
    xhr.open('get', path, true)
    xhr.addEventListener('load', function(event) {
        var status = event.currentTarget.status
        if (status == 200) {
            loadSVG(event.currentTarget.responseText)
        }
    })
    xhr.send()
}
function toDataUrl(url, callback) {
    var xhr = new XMLHttpRequest()
    xhr.onload = function() {
        var reader = new FileReader()
        reader.onloadend = function() {
            callback(reader.result)
        }
        reader.readAsDataURL(xhr.response)
    }
    xhr.open('GET', url)
    xhr.responseType = 'blob'
    xhr.send()
}
