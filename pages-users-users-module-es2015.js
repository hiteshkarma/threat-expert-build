(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-users-users-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-add/users-add.component.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-add/users-add.component.html ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"py-4\">\r\n    <div class=\"pages-header\">\r\n        <h4>{{isUpdate ? 'Update' : 'Add'}} Agent</h4>\r\n        <div>\r\n            <a [routerLink]=\"[ '/home/users']\" class=\"btn btn-secondary sbt-btn mr-3\">Cancel</a>\r\n            <button type=\"button\" class=\"btn btn-primary sbt-btn\" (click)=\"formSubmit()\">Save</button>\r\n        </div>\r\n    </div>\r\n</section>\r\n<section class=\"add-user-form agent-form\">\r\n    <form [formGroup]=\"userForm\" (ngSubmit)=\"formSubmit()\">\r\n        <div class=\"user-type\">\r\n            <h4 class=\"mb-5\">Agent Type</h4>\r\n            <div class=\"radio\">\r\n                <label class=\"pl-40\">\r\n                    <input type=\"radio\" [value]=\"'Full-time'\" formControlName=\"user_type\">\r\n                    <div>\r\n                        <h4 class=\"my-0\">Full-Time</h4>\r\n                    </div>\r\n                </label>\r\n                <label class=\"pl-40\">\r\n                    <input type=\"radio\" [value]=\"'Occasional'\" formControlName=\"user_type\">\r\n                    <div>\r\n                        <h4 class=\"my-0\">Temporary </h4>\r\n                    </div>\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\" *ngIf=\"userForm.get('user_type').value === 'Occasional'\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Number Of Days <!--<span class=\"text-danger\">*</span>--></label>\r\n            <div>\r\n                <input type=\"number\" formControlName=\"time_limit\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Number OF Days\">\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Role Name <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <select name=\"role\" formControlName=\"role\" class=\"form-control\" id=\"role\" style=\"max-height: 35px;\"\r\n                        [class.is-invalid]=\"userForm.controls.role.invalid && (userForm.controls.role.dirty || userForm.controls.role.touched)\">\r\n                    <option value=\"\">Select Role</option>\r\n                    <option *ngFor=\"let role of rolesList\" [value]=\"role?.id\">{{role?.title}}</option>\r\n                </select>\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.role.invalid && (userForm.controls.role.dirty || userForm.controls.role.touched)\">Invalid Role</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Assigner Name <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <select name=\"role\" formControlName=\"assigner\" class=\"form-control\" id=\"assigner\" style=\"max-height: 35px;\"\r\n                        [class.is-invalid]=\"userForm.controls.assigner.invalid && (userForm.controls.assigner.dirty || userForm.controls.assigner.touched)\">\r\n                    <option value=\"\">Select Assigner</option>\r\n                    <option *ngFor=\"let user of usersList\" [value]=\"user?.id\">{{user?.username}}</option>\r\n                </select>\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.assigner.invalid && (userForm.controls.assigner.dirty || userForm.controls.assigner.touched)\">Invalid Assigner</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">User Name <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"text\" formControlName=\"username\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"User Name\"\r\n                       [class.is-invalid]=\"userForm.controls.username.invalid && (userForm.controls.username.dirty || userForm.controls.username.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.username.invalid && (userForm.controls.username.dirty || userForm.controls.username.touched)\">Invalid username</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Email <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"text\" formControlName=\"email\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Email ID\"\r\n                       [class.is-invalid]=\"userForm.controls.email.invalid && (userForm.controls.email.dirty || userForm.controls.email.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.email.invalid && (userForm.controls.email.dirty || userForm.controls.email.touched)\">Invalid email ID</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\" *ngIf=\"!isUpdate\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Password\"\r\n                       [class.is-invalid]=\"userForm.controls.password.invalid && (userForm.controls.password.dirty || userForm.controls.password.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.password.invalid && (userForm.controls.password.dirty || userForm.controls.password.touched)\">Invalid password</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\" *ngIf=\"!isUpdate\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"password\" formControlName=\"cpassword\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Confirm Password\"\r\n                       [class.is-invalid]=\"userForm.controls.cpassword.invalid && (userForm.controls.cpassword.dirty || userForm.controls.cpassword.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"userForm.controls.cpassword.invalid && (userForm.controls.cpassword.dirty || userForm.controls.cpassword.touched)\">Invalid confirm password</p>\r\n                <p class=\"error-msg\" *ngIf=\"userForm.errors?.invalid\">Password Not Match</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"text-center\">\r\n            <button type=\"button\" class=\"btn btn-default\" [disabled]=\"userForm.invalid\" (click)=\"showPositionDialog()\">{{isUpdate ? 'Update' : 'Submit'}}</button>\r\n        </div>\r\n    </form>\r\n</section>\r\n<p-dialog header=\"Please Enter Password *\" [(visible)]=\"displayPosition\" [modal]=\"true\" [style]=\"{width: '50vw'}\" [baseZIndex]=\"10000\"\r\n          [draggable]=\"false\" [resizable]=\"false\">\r\n    <form [formGroup]=\"userPasswordCheckForm\" (ngSubmit)=\"passwordCheckSubmit()\">\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Password\"\r\n                       [class.is-invalid]=\"userPasswordCheckForm.controls.password.invalid && (userPasswordCheckForm.controls.password.dirty || userPasswordCheckForm.controls.password.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"userPasswordCheckForm.controls.password.invalid && (userPasswordCheckForm.controls.password.dirty || userPasswordCheckForm.controls.password.touched)\">Invalid password</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"text-center\">\r\n            <button class=\"btn btn-default\" [disabled]=\"userPasswordCheckForm.invalid\">Submit</button>\r\n        </div>\r\n    </form>\r\n</p-dialog>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-list/users-list.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-list/users-list.component.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"user-table-section\">\r\n    <div class=\"pages-header\">\r\n        <h4>Agents</h4>\r\n        <a [routerLink]=\"[ '/home/users/add-user']\" class=\"btn btn-primary sbt-btn\" *ngIf=\"jwtService.IsParentChildPermission('View Users', 'Create Users')\">New Agent</a>\r\n    </div>\r\n    <div class=\"card table-main user-table-main\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-9\">\r\n                <div class=\"user-table mt-4\">\r\n                    <p-table [value]=\"usersList\" [paginator]=\"true\" [rows]=\"10\" [showCurrentPageReport]=\"true\" [rowsPerPageOptions]=\"[10,25,50]\">\r\n                        <ng-template pTemplate=\"header\">\r\n                            <tr>\r\n                                <th>S. No.</th>\r\n                                <th>Username</th>\r\n                                <th>Email</th>\r\n                                <th>Soft Deleted</th>\r\n                                <th></th>\r\n                            </tr>\r\n                        </ng-template>\r\n                        <ng-template pTemplate=\"body\" let-user let-i=\"rowIndex\">\r\n                            <tr [routerLink]=\"[ '/home/users/edit-user', user?.id]\" *ngIf=\"jwtService.IsParentChildPermission('View Users', 'Edit Users')\">\r\n                                <td>{{i + 1}}</td>\r\n                                <td>{{user?.username}}</td>\r\n                                <td>{{user?.email}}</td>\r\n                                <td>{{user?.isDeleted ? 'Deleted' : 'N/A'}}</td>\r\n                                <td>\r\n                                    <span *ngIf=\"jwtService.IsParentChildPermission('View Users', 'Delete Users')\" class=\"delete text-danger\" title=\"Delete User\" (click)=\"deleteUser($event, user?.id)\">\r\n                                        <i class=\"pi pi-trash\"></i>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr *ngIf=\"!jwtService.IsParentChildPermission('View Users', 'Edit Users')\">\r\n                                <td>{{i + 1}}</td>\r\n                                <td>{{user?.username}}</td>     \r\n                                <td>{{user?.email}}</td>\r\n                                <td>{{user?.isDeleted ? 'Deleted' : 'N/A'}}</td>\r\n                                <td>\r\n                                    <span *ngIf=\"jwtService.IsParentChildPermission('View Users', 'Delete Users')\" class=\"delete text-danger\" title=\"Delete User\" (click)=\"deleteUser($event, user?.id)\">\r\n                                        <i class=\"pi pi-trash\"></i>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                        </ng-template>\r\n                        <!-- <ng-template pTemplate=\"paginatorleft\">\r\n                            <p-button type=\"button\" icon=\"pi pi-plus\" styleClass=\"p-button-text\"></p-button>\r\n                        </ng-template>\r\n                        <ng-template pTemplate=\"paginatorright\">\r\n                            <p-button type=\"button\" icon=\"pi pi-cloud\" styleClass=\"p-button-text\"></p-button>\r\n                        </ng-template> -->\r\n                    </p-table>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"list-desc\">\r\n                    <h3>Agents</h3>\r\n                    <p class=\"mb-0\">The list shows all the Agents added in your help desk.</p>\r\n                    <p>You can add new agents by clicking on \"New Agent\" Button.</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<p-confirmDialog header=\"Confirmation\" icon=\"pi pi-exclamation-triangle\"></p-confirmDialog>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/users-layout/users-layout.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/users-layout/users-layout.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"page-content\">\n    <router-outlet></router-outlet>\n</section>\n");

/***/ }),

/***/ "./src/app/home-layout/pages/users/component/users-add/users-add.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/component/users-add/users-add.component.css ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3VzZXJzL2NvbXBvbmVudC91c2Vycy1hZGQvdXNlcnMtYWRkLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/home-layout/pages/users/component/users-add/users-add.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/component/users-add/users-add.component.ts ***!
  \************************************************************************************/
/*! exports provided: UsersAddComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersAddComponent", function() { return UsersAddComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth-service.service */ "./src/app/services/auth-service.service.ts");
/* harmony import */ var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/utility.service */ "./src/app/services/utility.service.ts");
/* harmony import */ var _services_app_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../../services/app.service */ "./src/app/services/app.service.ts");









let UsersAddComponent = class UsersAddComponent {
    constructor(fb, authService, toastr, router, route, service, utility) {
        this.fb = fb;
        this.authService = authService;
        this.toastr = toastr;
        this.router = router;
        this.route = route;
        this.service = service;
        this.utility = utility;
        this.rolesList = [];
        this.usersList = [];
        this.isUpdate = false;
        this.route.params
            .pipe()
            .subscribe(param => {
            this.userId = param.id;
            if (this.userId) {
                this.isUpdate = true;
                this.getUserDetails(this.userId);
            }
        });
    }
    ngOnInit() {
        this.inituserForm();
        this.getAllUsers();
        this.getAllRoles();
        if (this.isUpdate) {
            this.userForm.get('id').setValue(this.userId);
            this.userForm.removeControl('password'); // or clearValidators()
            this.userForm.removeControl('cpassword');
        }
    }
    getAllUsers() {
        this.service.getAllUsers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
            .subscribe(response => {
            this.usersList = response;
        }, error => {
            this.toastr.errorToastr(error);
        });
    }
    getUserDetails(id) {
        this.service.getUserDetails(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
            .subscribe(response => {
            this.userDetails = response;
            this.userForm.patchValue(this.userDetails);
        }, error => {
            this.toastr.errorToastr(error);
        });
    }
    getAllRoles() {
        this.service.getAllRole().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
            .subscribe(response => {
            this.rolesList = response;
        }, error => {
            this.toastr.errorToastr(error);
        });
    }
    inituserForm() {
        this.userPasswordCheckForm = this.fb.group({
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
        });
        this.userForm = this.fb.group({
            id: [''],
            user_type: ['Full-time', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            time_limit: [1],
            username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            role: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            assigner: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            cpassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
        }, { validator: this.passwordConfirming });
    }
    formSubmit() {
        if (this.userForm.valid) {
            if (this.isUpdate) {
                this.showPositionDialog();
                return false;
            }
            delete this.userForm.value.cpassword;
            this.service.saveUser(this.userForm.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                .subscribe(response => {
                this.toastr.successToastr(!this.isUpdate ? 'Agent added successfully' : 'Agent updated successfully');
                this.router.navigateByUrl('/home/users/user-listing');
                this.displayPosition = false;
            }, error => {
                this.toastr.errorToastr(error);
            });
        }
        else {
            this.utility.validateAllFormFields(this.userForm);
        }
    }
    FinalUpdateAfterPasswordVerify() {
        if (this.userForm.valid) {
            delete this.userForm.value.cpassword;
            this.service.saveUser(this.userForm.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                .subscribe(response => {
                this.toastr.successToastr(!this.isUpdate ? 'Agent added successfully' : 'Agent updated successfully');
                this.router.navigateByUrl('/home/users/user-listing');
                this.displayPosition = false;
            }, error => {
                this.toastr.errorToastr(error);
            });
        }
        else {
            this.utility.validateAllFormFields(this.userForm);
        }
    }
    passwordCheckSubmit() {
        if (this.userPasswordCheckForm.valid) {
            this.service.passwordCheck(this.userPasswordCheckForm.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                .subscribe(response => {
                if (!response.status) {
                    this.toastr.errorToastr('Password Incorrect');
                    return;
                }
                this.FinalUpdateAfterPasswordVerify();
            }, error => {
                this.toastr.errorToastr(error);
            });
        }
        else {
            this.utility.validateAllFormFields(this.userPasswordCheckForm);
        }
    }
    passwordConfirming(c) {
        if (!(c.get('password') && c.get('password').value)) {
            c.setErrors(null);
        }
        else {
            if (c.get('password').value !== c.get('cpassword').value) {
                return { invalid: true };
            }
        }
    }
    showPositionDialog() {
        this.displayPosition = true;
    }
};
UsersAddComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_6__["AuthServiceService"] },
    { type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _services_app_service__WEBPACK_IMPORTED_MODULE_8__["AppService"] },
    { type: src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_7__["UtilityService"] }
];
UsersAddComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-users-add',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./users-add.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-add/users-add.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./users-add.component.css */ "./src/app/home-layout/pages/users/component/users-add/users-add.component.css")).default]
    })
], UsersAddComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/users/component/users-list/users-list.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/component/users-list/users-list.component.css ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3VzZXJzL2NvbXBvbmVudC91c2Vycy1saXN0L3VzZXJzLWxpc3QuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/home-layout/pages/users/component/users-list/users-list.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/component/users-list/users-list.component.ts ***!
  \**************************************************************************************/
/*! exports provided: UsersListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersListComponent", function() { return UsersListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_app_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../../services/app.service */ "./src/app/services/app.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var src_app_services_jwt_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/jwt.service */ "./src/app/services/jwt.service.ts");









let UsersListComponent = class UsersListComponent {
    constructor(toastr, router, jwtService, confirmationService, service) {
        this.toastr = toastr;
        this.router = router;
        this.jwtService = jwtService;
        this.confirmationService = confirmationService;
        this.service = service;
        this.first = 0;
        this.rows = 10;
        this.usersList = [];
        // variable used to unsubscribe
        // tslint:disable-next-line:variable-name
        this._unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    ngOnInit() {
        this.usersList = [
            {
                name: 'anil',
                no: '123',
                email: 'abc123@gmail.com'
            },
            {
                name: 'saurabh',
                no: '123',
                email: 'abc123@gmail.com'
            }
        ];
        this.getAllUsers();
    }
    getAllUsers() {
        this.service.getAllUsers().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
            .subscribe(response => {
            this.usersList = response;
        }, error => {
            this.toastr.errorToastr(error.error.message);
        });
    }
    next() {
        this.first = this.first + this.rows;
    }
    prev() {
        this.first = this.first - this.rows;
    }
    reset() {
        this.first = 0;
    }
    isLastPage() {
        return this.usersList ? this.first === (this.usersList.length - this.rows) : true;
    }
    isFirstPage() {
        return this.usersList ? this.first === 0 : true;
    }
    deleteUser(event, userId) {
        event.preventDefault();
        event.stopPropagation();
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete this user?',
            accept: () => {
                this.service.deleteUsers(userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])())
                    .subscribe(response => {
                    this.toastr.successToastr('User deleted successfully');
                    this.getAllUsers();
                }, error => {
                    this.toastr.errorToastr(error.error.message);
                });
                // Actual logic to perform a confirmation
            }
        });
    }
    // unsubscribe function----------
    // tslint:disable-next-line:use-lifecycle-interface
    ngOnDestroy() {
        this._unsubscribe.next(true);
        this._unsubscribe.complete();
    }
};
UsersListComponent.ctorParameters = () => [
    { type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_services_jwt_service__WEBPACK_IMPORTED_MODULE_8__["JwtService"] },
    { type: primeng_api__WEBPACK_IMPORTED_MODULE_7__["ConfirmationService"] },
    { type: _services_app_service__WEBPACK_IMPORTED_MODULE_6__["AppService"] }
];
UsersListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-users-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./users-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/component/users-list/users-list.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./users-list.component.css */ "./src/app/home-layout/pages/users/component/users-list/users-list.component.css")).default]
    })
], UsersListComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/users/users-layout/users-layout.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/users-layout/users-layout.component.css ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3VzZXJzL3VzZXJzLWxheW91dC91c2Vycy1sYXlvdXQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/home-layout/pages/users/users-layout/users-layout.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/home-layout/pages/users/users-layout/users-layout.component.ts ***!
  \********************************************************************************/
/*! exports provided: UsersLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersLayoutComponent", function() { return UsersLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let UsersLayoutComponent = class UsersLayoutComponent {
    constructor() { }
    ngOnInit() {
    }
};
UsersLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-users-layout',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./users-layout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/users/users-layout/users-layout.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./users-layout.component.css */ "./src/app/home-layout/pages/users/users-layout/users-layout.component.css")).default]
    })
], UsersLayoutComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/users/users-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/home-layout/pages/users/users-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: UsersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersRoutingModule", function() { return UsersRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _users_layout_users_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./users-layout/users-layout.component */ "./src/app/home-layout/pages/users/users-layout/users-layout.component.ts");
/* harmony import */ var _component_users_list_users_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./component/users-list/users-list.component */ "./src/app/home-layout/pages/users/component/users-list/users-list.component.ts");
/* harmony import */ var _component_users_add_users_add_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/users-add/users-add.component */ "./src/app/home-layout/pages/users/component/users-add/users-add.component.ts");






const routes = [
    {
        path: '',
        component: _users_layout_users_layout_component__WEBPACK_IMPORTED_MODULE_3__["UsersLayoutComponent"],
        children: [
            {
                path: 'user-listing',
                component: _component_users_list_users_list_component__WEBPACK_IMPORTED_MODULE_4__["UsersListComponent"]
            },
            {
                path: 'add-user',
                component: _component_users_add_users_add_component__WEBPACK_IMPORTED_MODULE_5__["UsersAddComponent"]
            },
            {
                path: 'edit-user/:id',
                component: _component_users_add_users_add_component__WEBPACK_IMPORTED_MODULE_5__["UsersAddComponent"]
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'user-listing'
            },
        ]
    }
];
let UsersRoutingModule = class UsersRoutingModule {
};
UsersRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], UsersRoutingModule);



/***/ }),

/***/ "./src/app/home-layout/pages/users/users.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/home-layout/pages/users/users.module.ts ***!
  \*********************************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _users_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./users-routing.module */ "./src/app/home-layout/pages/users/users-routing.module.ts");
/* harmony import */ var _users_layout_users_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./users-layout/users-layout.component */ "./src/app/home-layout/pages/users/users-layout/users-layout.component.ts");
/* harmony import */ var _component_users_list_users_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./component/users-list/users-list.component */ "./src/app/home-layout/pages/users/component/users-list/users-list.component.ts");
/* harmony import */ var _component_users_add_users_add_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./component/users-add/users-add.component */ "./src/app/home-layout/pages/users/component/users-add/users-add.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");









let UsersModule = class UsersModule {
};
UsersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_users_layout_users_layout_component__WEBPACK_IMPORTED_MODULE_4__["UsersLayoutComponent"], _component_users_list_users_list_component__WEBPACK_IMPORTED_MODULE_5__["UsersListComponent"], _component_users_add_users_add_component__WEBPACK_IMPORTED_MODULE_6__["UsersAddComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _users_routing_module__WEBPACK_IMPORTED_MODULE_3__["UsersRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"]
        ]
    })
], UsersModule);



/***/ })

}]);
//# sourceMappingURL=pages-users-users-module-es2015.js.map