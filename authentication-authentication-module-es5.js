function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["authentication-authentication-module"], {
  /***/
  "./node_modules/ng-otp-input/fesm2015/ng-otp-input.js":
  /*!************************************************************!*\
    !*** ./node_modules/ng-otp-input/fesm2015/ng-otp-input.js ***!
    \************************************************************/

  /*! exports provided: NgOtpInputModule, ɵa, ɵb, ɵc */

  /***/
  function node_modulesNgOtpInputFesm2015NgOtpInputJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NgOtpInputModule", function () {
      return NgOtpInputModule;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵa", function () {
      return NgOtpInputComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵb", function () {
      return KeysPipe;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ɵc", function () {
      return NumberOnlyDirective;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var KeysPipe = /*#__PURE__*/function () {
      function KeysPipe() {
        _classCallCheck(this, KeysPipe);
      }

      _createClass(KeysPipe, [{
        key: "transform",
        value:
        /**
         * @param {?} value
         * @return {?}
         */
        function transform(value) {
          return Object.keys(value);
        }
      }]);

      return KeysPipe;
    }();

    KeysPipe.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
      args: [{
        name: 'keys'
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var Config = function Config() {
      _classCallCheck(this, Config);
    };

    if (false) {}
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var NgOtpInputComponent = /*#__PURE__*/function () {
      /**
       * @param {?} keysPipe
       */
      function NgOtpInputComponent(keysPipe) {
        _classCallCheck(this, NgOtpInputComponent);

        this.keysPipe = keysPipe;
        this.config = {
          length: 4
        }; // tslint:disable-next-line: no-output-on-prefix

        this.onInputChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.inputControls = new Array(this.config.length);
        this.componentKey = Math.random().toString(36).substring(2) + new Date().getTime().toString(36);
      }
      /**
       * @return {?}
       */


      _createClass(NgOtpInputComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.otpForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({});

          for (var index = 0; index < this.config.length; index++) {
            this.otpForm.addControl(this.getControlName(index), new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]());
          }

          this.inputType = this.getInputType();
        }
        /**
         * @return {?}
         */

      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          var _this = this;

          if (!this.config.disableAutoFocus) {
            /** @type {?} */
            var containerItem = document.getElementById("c_".concat(this.componentKey));

            if (containerItem) {
              containerItem.addEventListener('paste',
              /**
              * @param {?} evt
              * @return {?}
              */
              function (evt) {
                return _this.handlePaste(evt);
              });
              /** @type {?} */

              var ele = containerItem.getElementsByClassName('otp-input')[0];

              if (ele && ele.focus) {
                ele.focus();
              }
            }
          }
        }
        /**
         * @private
         * @param {?} idx
         * @return {?}
         */

      }, {
        key: "getControlName",
        value: function getControlName(idx) {
          return "ctrl_".concat(idx);
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "ifLeftArrow",
        value: function ifLeftArrow(event) {
          return this.ifKeyCode(event, 37);
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "ifRightArrow",
        value: function ifRightArrow(event) {
          return this.ifKeyCode(event, 39);
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "ifBackspaceOrDelete",
        value: function ifBackspaceOrDelete(event) {
          return event.key === 'Backspace' || event.key === 'Delete' || this.ifKeyCode(event, 8) || this.ifKeyCode(event, 46);
        }
        /**
         * @param {?} event
         * @param {?} targetCode
         * @return {?}
         */

      }, {
        key: "ifKeyCode",
        value: function ifKeyCode(event, targetCode) {
          /** @type {?} */
          var key = event.keyCode || event.charCode; // tslint:disable-next-line: triple-equals

          return key == targetCode ? true : false;
        }
        /**
         * @param {?} $event
         * @return {?}
         */

      }, {
        key: "onKeyDown",
        value: function onKeyDown($event) {
          /** @type {?} */
          var isSpace = this.ifKeyCode($event, 32);

          if (isSpace) {
            // prevent space
            return false;
          }
        }
        /**
         * @param {?} $event
         * @param {?} inputIdx
         * @return {?}
         */

      }, {
        key: "onKeyUp",
        value: function onKeyUp($event, inputIdx) {
          /** @type {?} */
          var nextInputId = this.appendKey("otp_".concat(inputIdx + 1));
          /** @type {?} */

          var prevInputId = this.appendKey("otp_".concat(inputIdx - 1));

          if (this.ifRightArrow($event)) {
            this.setSelected(nextInputId);
            return;
          }

          if (this.ifLeftArrow($event)) {
            this.setSelected(prevInputId);
            return;
          }
          /** @type {?} */


          var isBackspace = this.ifBackspaceOrDelete($event);

          if (isBackspace && !$event.target.value) {
            this.setSelected(prevInputId);
            this.rebuildValue();
            return;
          }

          if (!$event.target.value) {
            return;
          }

          if (this.ifValidEntry($event)) {
            this.setSelected(nextInputId);
          }

          this.rebuildValue();
        }
        /**
         * @param {?} id
         * @return {?}
         */

      }, {
        key: "appendKey",
        value: function appendKey(id) {
          return "".concat(id, "_").concat(this.componentKey);
        }
        /**
         * @param {?} eleId
         * @return {?}
         */

      }, {
        key: "setSelected",
        value: function setSelected(eleId) {
          this.focusTo(eleId);
          /** @type {?} */

          var ele = document.getElementById(eleId);

          if (ele && ele.setSelectionRange) {
            setTimeout(
            /**
            * @return {?}
            */
            function () {
              ele.setSelectionRange(0, 1);
            }, 0);
          }
        }
        /**
         * @param {?} event
         * @return {?}
         */

      }, {
        key: "ifValidEntry",
        value: function ifValidEntry(event) {
          /** @type {?} */
          var inp = String.fromCharCode(event.keyCode);
          /** @type {?} */

          var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
          return isMobile || /[a-zA-Z0-9-_]/.test(inp) || this.config.allowKeyCodes && this.config.allowKeyCodes.includes(event.keyCode) || event.keyCode >= 96 && event.keyCode <= 105;
        }
        /**
         * @param {?} eleId
         * @return {?}
         */

      }, {
        key: "focusTo",
        value: function focusTo(eleId) {
          /** @type {?} */
          var ele = document.getElementById(eleId);

          if (ele) {
            ele.focus();
          }
        } // method to set component value

        /**
         * @param {?} value
         * @return {?}
         */

      }, {
        key: "setValue",
        value: function setValue(value) {
          var _this2 = this;

          if (this.config.allowNumbersOnly && isNaN(value)) {
            return;
          }

          this.otpForm.reset();

          if (!value) {
            this.rebuildValue();
            return;
          }

          value = value.toString().replace(/\s/g, ''); // remove whitespace

          Array.from(value).forEach(
          /**
          * @param {?} c
          * @param {?} idx
          * @return {?}
          */
          function (c, idx) {
            if (_this2.otpForm.get(_this2.getControlName(idx))) {
              _this2.otpForm.get(_this2.getControlName(idx)).setValue(c);
            }
          });

          if (!this.config.disableAutoFocus) {
            /** @type {?} */
            var containerItem = document.getElementById("c_".concat(this.componentKey));
            /** @type {?} */

            var indexOfElementToFocus = value.length < this.config.length ? value.length : this.config.length - 1;
            /** @type {?} */

            var ele = containerItem.getElementsByClassName('otp-input')[indexOfElementToFocus];

            if (ele && ele.focus) {
              ele.focus();
            }
          }

          this.rebuildValue();
        }
        /**
         * @return {?}
         */

      }, {
        key: "rebuildValue",
        value: function rebuildValue() {
          var _this3 = this;

          /** @type {?} */
          var val = '';
          this.keysPipe.transform(this.otpForm.controls).forEach(
          /**
          * @param {?} k
          * @return {?}
          */
          function (k) {
            if (_this3.otpForm.controls[k].value) {
              val += _this3.otpForm.controls[k].value;
            }
          });
          this.onInputChange.emit(val);
        }
        /**
         * @return {?}
         */

      }, {
        key: "getInputType",
        value: function getInputType() {
          return this.config.isPasswordInput ? 'password' : this.config.allowNumbersOnly ? 'tel' : 'text';
        }
        /**
         * @param {?} e
         * @return {?}
         */

      }, {
        key: "handlePaste",
        value: function handlePaste(e) {
          // Get pasted data via clipboard API

          /** @type {?} */
          var clipboardData = e.clipboardData || window['clipboardData'];

          if (clipboardData) {
            /** @type {?} */
            var pastedData = clipboardData.getData('Text');
          } // Stop data actually being pasted into div


          e.stopPropagation();
          e.preventDefault();

          if (!pastedData) {
            return;
          }

          this.setValue(pastedData);
        }
      }]);

      return NgOtpInputComponent;
    }();

    NgOtpInputComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        // tslint:disable-next-line: component-selector
        selector: 'ng-otp-input',
        template: "<div class=\"wrapper {{config.containerClass}}\" id=\"c_{{componentKey}}\" *ngIf=\"otpForm?.controls\"\r\n  [ngStyle]=\"config.containerStyles\">\r\n  <input [pattern]=\"config.allowNumbersOnly ? '\\\\d*' : ''\" [type]=\"inputType\" numberOnly [placeholder]=\"config?.placeholder || ''\"\r\n    [disabledNumberOnly]=\"!config.allowNumbersOnly\" [ngStyle]=\"config.inputStyles\" maxlength=\"1\"\r\n    class=\"otp-input {{config.inputClass}}\" autocomplete=\"off\" *ngFor=\"let item of otpForm?.controls | keys;let i=index\"\r\n    [formControl]=\"otpForm.controls[item]\" id=\"otp_{{i}}_{{componentKey}}\" (keydown)=\"onKeyDown($event)\"\r\n    (keyup)=\"onKeyUp($event,i)\">\r\n</div>",
        styles: [".otp-input{width:50px;height:50px;border-radius:4px;border:1px solid #c5c5c5;text-align:center;font-size:32px}.wrapper .otp-input:not(:last-child){margin-right:8px}@media screen and (max-width:767px){.otp-input{width:40px;font-size:24px;height:40px}}@media screen and (max-width:420px){.otp-input{width:30px;font-size:18px;height:30px}}"]
      }]
    }];
    /** @nocollapse */

    NgOtpInputComponent.ctorParameters = function () {
      return [{
        type: KeysPipe
      }];
    };

    NgOtpInputComponent.propDecorators = {
      config: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      onInputChange: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }]
    };

    if (false) {}
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var NumberOnlyDirective = /*#__PURE__*/function () {
      /**
       * @param {?} _elRef
       * @param {?} _renderer
       */
      function NumberOnlyDirective(_elRef, _renderer) {
        _classCallCheck(this, NumberOnlyDirective);

        this._elRef = _elRef;
        this._renderer = _renderer;
      }
      /**
       * @return {?}
       */


      _createClass(NumberOnlyDirective, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          if (!this.disabledNumberOnly) {
            this._renderer.setAttribute(this._elRef.nativeElement, 'onkeypress', 'return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 0');
          }
        }
      }]);

      return NumberOnlyDirective;
    }();

    NumberOnlyDirective.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"],
      args: [{
        selector: '[numberOnly]'
      }]
    }];
    /** @nocollapse */

    NumberOnlyDirective.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
      }];
    };

    NumberOnlyDirective.propDecorators = {
      disabledNumberOnly: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }]
    };

    if (false) {}
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var NgOtpInputModule = function NgOtpInputModule() {
      _classCallCheck(this, NgOtpInputModule);
    };

    NgOtpInputModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"]],
        declarations: [NgOtpInputComponent, KeysPipe, NumberOnlyDirective],
        exports: [NgOtpInputComponent],
        providers: [KeysPipe]
      }]
    }];
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    //# sourceMappingURL=ng-otp-input.js.map

    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/authentication-layout/authentication-layout.component.html":
  /*!*********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/authentication-layout/authentication-layout.component.html ***!
    \*********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthenticationAuthenticationLayoutAuthenticationLayoutComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"auth-main\">\n    <div class=\"auth-left\">\n        <div class=\"brand-logo\">\n            <h1>Threat Expert</h1>\n            <p>Artificial Intelligence Powered Digital Risk Management</p>\n        </div>\n        <div class=\"powered-by\">\n            <p>powered by</p>\n        </div>\n    </div>\n    <div class=\"auth-right\">\n        <div class=\"w-100\">\n            <router-outlet></router-outlet>\n        </div>\n    </div>\n</section>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/change-password/change-password.component.html":
  /*!********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/change-password/change-password.component.html ***!
    \********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthenticationComponentsChangePasswordChangePasswordComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"auth-form-main\">\r\n    <div class=\"auth-form text-left\">\r\n        <div class=\"form-heading\">\r\n            <h2>Change Password</h2>\r\n        </div>\r\n        <form [formGroup]=\"changePasswordForm\" (ngSubmit)=\"formSubmit()\">\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Current Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"old_password\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"changePasswordForm.controls.old_password.invalid && (changePasswordForm.controls.old_password.dirty || changePasswordForm.controls.old_password.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"changePasswordForm.controls.old_password.invalid && (changePasswordForm.controls.old_password.dirty || changePasswordForm.controls.old_password.touched)\"\r\n                    >Current password Required</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"changePasswordForm.controls.password.invalid && (changePasswordForm.controls.password.dirty || changePasswordForm.controls.password.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"changePasswordForm.controls.password.invalid && (changePasswordForm.controls.password.dirty || changePasswordForm.controls.password.touched)\"\r\n                    >Invalid password</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Confirm Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"cpassword\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"changePasswordForm.controls.cpassword.invalid && (changePasswordForm.controls.cpassword.dirty || changePasswordForm.controls.cpassword.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"changePasswordForm.controls.cpassword.invalid && (changePasswordForm.controls.cpassword.dirty || changePasswordForm.controls.cpassword.touched)\"\r\n                    >Invalid confirm password</p>\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"passwordNotMatch\"\r\n                    >Password and Confirm password are not same</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button class=\"btn btn-default\" [disabled]=\"changePasswordForm.invalid\">Submit</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</section>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/forgot-password/forgot-password.component.html":
  /*!********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/forgot-password/forgot-password.component.html ***!
    \********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthenticationComponentsForgotPasswordForgotPasswordComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"auth-form-main\">\r\n    <div class=\"auth-form text-left\" *ngIf=\"!showOtpComponent\">\r\n        <div class=\"form-heading\">\r\n            <h2>Forgot Password</h2>\r\n        </div>\r\n        <form [formGroup]=\"forgotForm\" (ngSubmit)=\"formSubmit()\">\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Email ID</label>\r\n                <div>\r\n                    <input type=\"text\" formControlName=\"email\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"forgotForm.controls.email.invalid && (forgotForm.controls.email.dirty || forgotForm.controls.email.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"forgotForm.controls.email.invalid && (forgotForm.controls.email.dirty || forgotForm.controls.email.touched)\"\r\n                    >Invalid Email ID</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-right mb-2\">\r\n                <div class=\"sub-link\">\r\n                    <a [routerLink]=\"[ '/authentication/login']\">Back to Login</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button class=\"btn btn-default\" [disabled]=\"forgotForm.invalid\">Submit</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n    <div class=\"auth-form text-left\" *ngIf=\"showOtpComponent\">\r\n        <div class=\"form-heading\">\r\n            <h2>Enter Otp</h2>\r\n        </div>\r\n        <form [formGroup]=\"forgotForm\" (ngSubmit)=\"formOtpSubmit()\">\r\n            <div class=\"form-group\">\r\n                <ng-otp-input #ngOtpInput (onInputChange)=\"onOtpChange($event)\" [config]=\"config\"></ng-otp-input>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button class=\"btn btn-default\" [disabled]=\"forgotForm.invalid\">Submit</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</section>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/login/login.component.html":
  /*!************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/login/login.component.html ***!
    \************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthenticationComponentsLoginLoginComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"auth-form-main\">\r\n    <div class=\"auth-form text-left\">\r\n        <div class=\"form-heading\">\r\n            <h2>Login</h2>\r\n        </div>\r\n        <form [formGroup]=\"loginForm\" (ngSubmit)=\"formSubmit()\">\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Email ID</label>\r\n                <div>\r\n                    <input type=\"text\" formControlName=\"email\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"loginForm.controls.email.invalid && (loginForm.controls.email.dirty || loginForm.controls.email.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"loginForm.controls.email.invalid && (loginForm.controls.email.dirty || loginForm.controls.email.touched)\"\r\n                    >Invalid Email ID</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"loginForm.controls.password.invalid && (loginForm.controls.password.dirty || loginForm.controls.password.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"loginForm.controls.password.invalid && (loginForm.controls.password.dirty || loginForm.controls.password.touched)\"\r\n                    >Invalid passsword</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-right mb-2\">\r\n                <div class=\"sub-link\">\r\n                    <a [routerLink]=\"[ '/authentication/forgot-password']\">Forgot Password</a>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button class=\"btn btn-default\" [disabled]=\"loginForm.invalid\">\r\n                    Login\r\n                    <div class=\"spinner-border text-primary\" *ngIf=\"loading\" role=\"status\">\r\n                        <span class=\"sr-only\">Loading...</span>\r\n                    </div>\r\n                </button>\r\n            </div>\r\n        </form>\r\n        <div class=\"google-signin text-center\">\r\n            <button (click)=\"socialSignIn('google')\" class=\"btn btn-danger my-2\">Signin in with Google</button>  \r\n        </div>\r\n    </div>\r\n</section>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/reset-password/reset-password.component.html":
  /*!******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/reset-password/reset-password.component.html ***!
    \******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAuthenticationComponentsResetPasswordResetPasswordComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"auth-form-main\">\r\n    <div class=\"auth-form text-left\">\r\n        <div class=\"form-heading\">\r\n            <h2>Reset Password</h2>\r\n        </div>\r\n        <form [formGroup]=\"resetPasswordForm\" (ngSubmit)=\"formSubmit()\">\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"password\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"resetPasswordForm.controls.password.invalid && (resetPasswordForm.controls.password.dirty || resetPasswordForm.controls.password.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"resetPasswordForm.controls.password.invalid && (resetPasswordForm.controls.password.dirty || resetPasswordForm.controls.password.touched)\"\r\n                    >Invalid password</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Confirm Password</label>\r\n                <div>\r\n                    <input type=\"password\" formControlName=\"cpassword\" class=\"form-control\" style=\"max-height: 35px;\"\r\n                           [class.is-invalid]=\"resetPasswordForm.controls.cpassword.invalid && (resetPasswordForm.controls.cpassword.dirty || resetPasswordForm.controls.cpassword.touched)\"\r\n                    >\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"resetPasswordForm.controls.cpassword.invalid && (resetPasswordForm.controls.cpassword.dirty || resetPasswordForm.controls.cpassword.touched)\"\r\n                    >Invalid confirm password</p>\r\n                    <p class=\"error-msg\"\r\n                       *ngIf=\"!(resetPasswordForm.controls.cpassword.invalid && (resetPasswordForm.controls.cpassword.dirty || resetPasswordForm.controls.cpassword.touched)) && (passwordNotMatch)\"\r\n                    >Password and Confirm password are not same</p>\r\n                </div>\r\n            </div>\r\n            <div class=\"text-center\">\r\n                <button class=\"btn btn-default\" [disabled]=\"resetPasswordForm.invalid\">Submit</button>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</section>\r\n";
    /***/
  },

  /***/
  "./src/app/authentication/authentication-layout/authentication-layout.component.css":
  /*!******************************************************************************************!*\
    !*** ./src/app/authentication/authentication-layout/authentication-layout.component.css ***!
    \******************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthenticationAuthenticationLayoutAuthenticationLayoutComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".auth-main{\r\n    min-height: 500px;\r\n    height: 100vh;\r\n    width: 100%;\r\n    display: flex;\r\n}\r\n.auth-main > div{\r\n    height: 100%;\r\n    width: 50%;\r\n    display: inline-flex;\r\n    text-align: center;\r\n}\r\n.auth-left{\r\n    padding: 25px 10px;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    background: rgb(9,77,121);\r\n    background: linear-gradient(to bottom, rgba(9,77,121,0.7990546560421043) 0%, rgba(0,212,255,0.8494748241093313) 95%);\r\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\"#094d79\",endColorstr=\"#00d4ff\",GradientType=1);     \r\n    color: #fff;\r\n}\r\n.auth-left h1{\r\n    font-size: 30px;\r\n    font-weight: bold;\r\n}\r\n.auth-right{\r\n    align-items: center;\r\n    background-color: #f1f1f1;\r\n}\r\n@media (max-width:991px) {\r\n    .auth-main div.auth-left{\r\n      width: 35%;\r\n    }\r\n    .auth-main div.auth-right{\r\n        width: 65%;\r\n        padding: 0 15px;\r\n    }\r\n}\r\n@media (max-width:767px) {\r\n    .powered-by{\r\n        display: none;\r\n    }\r\n    .auth-main{\r\n        display: block;\r\n        background: rgb(9,77,121);\r\n        background: linear-gradient(to bottom, rgba(9,77,121,0.7990546560421043) 0%, rgba(0,212,255,0.8494748241093313) 95%);\r\n    }\r\n    .auth-main div.auth-left, .auth-main div.auth-right{\r\n        width: 100%;\r\n        height: auto;\r\n        background: transparent;\r\n    }\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24tbGF5b3V0L2F1dGhlbnRpY2F0aW9uLWxheW91dC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksWUFBWTtJQUNaLFVBQVU7SUFDVixvQkFBb0I7SUFDcEIsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5Qix5QkFBeUI7SUFHekIsb0hBQW9IO0lBQ3BILGdIQUFnSDtJQUNoSCxXQUFXO0FBQ2Y7QUFDQTtJQUNJLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQix5QkFBeUI7QUFDN0I7QUFFQTtJQUNJO01BQ0UsVUFBVTtJQUNaO0lBQ0E7UUFDSSxVQUFVO1FBQ1YsZUFBZTtJQUNuQjtBQUNKO0FBR0E7SUFDSTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGNBQWM7UUFDZCx5QkFBeUI7UUFDekIsb0hBQW9IO0lBQ3hIO0lBQ0E7UUFDSSxXQUFXO1FBQ1gsWUFBWTtRQUNaLHVCQUF1QjtJQUMzQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvYXV0aGVudGljYXRpb24vYXV0aGVudGljYXRpb24tbGF5b3V0L2F1dGhlbnRpY2F0aW9uLWxheW91dC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmF1dGgtbWFpbntcclxuICAgIG1pbi1oZWlnaHQ6IDUwMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxufVxyXG4uYXV0aC1tYWluID4gZGl2e1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDUwJTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5hdXRoLWxlZnR7XHJcbiAgICBwYWRkaW5nOiAyNXB4IDEwcHg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDksNzcsMTIxKTtcclxuICAgIGJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSg5LDc3LDEyMSwwLjc5OTA1NDY1NjA0MjEwNDMpIDAlLCByZ2JhKDAsMjEyLDI1NSwwLjg0OTQ3NDgyNDEwOTMzMTMpIDk1JSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoOSw3NywxMjEsMC43OTkwNTQ2NTYwNDIxMDQzKSAwJSwgcmdiYSgwLDIxMiwyNTUsMC44NDk0NzQ4MjQxMDkzMzEzKSA5NSUpO1xyXG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSg5LDc3LDEyMSwwLjc5OTA1NDY1NjA0MjEwNDMpIDAlLCByZ2JhKDAsMjEyLDI1NSwwLjg0OTQ3NDgyNDEwOTMzMTMpIDk1JSk7XHJcbiAgICBmaWx0ZXI6IHByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5ncmFkaWVudChzdGFydENvbG9yc3RyPVwiIzA5NGQ3OVwiLGVuZENvbG9yc3RyPVwiIzAwZDRmZlwiLEdyYWRpZW50VHlwZT0xKTsgICAgIFxyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuLmF1dGgtbGVmdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5hdXRoLXJpZ2h0e1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOjk5MXB4KSB7XHJcbiAgICAuYXV0aC1tYWluIGRpdi5hdXRoLWxlZnR7XHJcbiAgICAgIHdpZHRoOiAzNSU7XHJcbiAgICB9XHJcbiAgICAuYXV0aC1tYWluIGRpdi5hdXRoLXJpZ2h0e1xyXG4gICAgICAgIHdpZHRoOiA2NSU7XHJcbiAgICAgICAgcGFkZGluZzogMCAxNXB4O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6NzY3cHgpIHtcclxuICAgIC5wb3dlcmVkLWJ5e1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgICAuYXV0aC1tYWlue1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHJnYig5LDc3LDEyMSk7XHJcbiAgICAgICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIGJvdHRvbSwgcmdiYSg5LDc3LDEyMSwwLjc5OTA1NDY1NjA0MjEwNDMpIDAlLCByZ2JhKDAsMjEyLDI1NSwwLjg0OTQ3NDgyNDEwOTMzMTMpIDk1JSk7XHJcbiAgICB9XHJcbiAgICAuYXV0aC1tYWluIGRpdi5hdXRoLWxlZnQsIC5hdXRoLW1haW4gZGl2LmF1dGgtcmlnaHR7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiBhdXRvO1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */";
    /***/
  },

  /***/
  "./src/app/authentication/authentication-layout/authentication-layout.component.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/authentication/authentication-layout/authentication-layout.component.ts ***!
    \*****************************************************************************************/

  /*! exports provided: AuthenticationLayoutComponent */

  /***/
  function srcAppAuthenticationAuthenticationLayoutAuthenticationLayoutComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthenticationLayoutComponent", function () {
      return AuthenticationLayoutComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../core/services/auth.service */
    "./src/app/core/services/auth.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var AuthenticationLayoutComponent = /*#__PURE__*/function () {
      function AuthenticationLayoutComponent(auth, router) {
        _classCallCheck(this, AuthenticationLayoutComponent);

        this.auth = auth;
        this.router = router;

        if (this.auth.isAuthenticated()) {
          this.router.navigateByUrl('/home');
        }
      }

      _createClass(AuthenticationLayoutComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AuthenticationLayoutComponent;
    }();

    AuthenticationLayoutComponent.ctorParameters = function () {
      return [{
        type: _core_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    AuthenticationLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-authentication-layout',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./authentication-layout.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/authentication-layout/authentication-layout.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./authentication-layout.component.css */
      "./src/app/authentication/authentication-layout/authentication-layout.component.css"))["default"]]
    })], AuthenticationLayoutComponent);
    /***/
  },

  /***/
  "./src/app/authentication/authentication-routing.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/authentication/authentication-routing.module.ts ***!
    \*****************************************************************/

  /*! exports provided: AuthenticationRoutingModule */

  /***/
  function srcAppAuthenticationAuthenticationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthenticationRoutingModule", function () {
      return AuthenticationRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _authentication_layout_authentication_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./authentication-layout/authentication-layout.component */
    "./src/app/authentication/authentication-layout/authentication-layout.component.ts");
    /* harmony import */


    var _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/login/login.component */
    "./src/app/authentication/components/login/login.component.ts");
    /* harmony import */


    var _components_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/forgot-password/forgot-password.component */
    "./src/app/authentication/components/forgot-password/forgot-password.component.ts");
    /* harmony import */


    var _components_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/reset-password/reset-password.component */
    "./src/app/authentication/components/reset-password/reset-password.component.ts");
    /* harmony import */


    var _components_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./components/change-password/change-password.component */
    "./src/app/authentication/components/change-password/change-password.component.ts");

    var routes = [{
      path: '',
      component: _authentication_layout_authentication_layout_component__WEBPACK_IMPORTED_MODULE_3__["AuthenticationLayoutComponent"],
      children: [{
        path: 'login',
        component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
      }, {
        path: 'forgot-password',
        component: _components_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_5__["ForgotPasswordComponent"]
      }, {
        path: 'change-password',
        component: _components_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__["ChangePasswordComponent"]
      }, {
        path: 'reset-password',
        component: _components_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_6__["ResetPasswordComponent"]
      }, {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
      }]
    }];

    var AuthenticationRoutingModule = function AuthenticationRoutingModule() {
      _classCallCheck(this, AuthenticationRoutingModule);
    };

    AuthenticationRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AuthenticationRoutingModule);
    /***/
  },

  /***/
  "./src/app/authentication/authentication.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/authentication/authentication.module.ts ***!
    \*********************************************************/

  /*! exports provided: AuthenticationModule */

  /***/
  function srcAppAuthenticationAuthenticationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthenticationModule", function () {
      return AuthenticationModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _authentication_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./authentication-routing.module */
    "./src/app/authentication/authentication-routing.module.ts");
    /* harmony import */


    var _authentication_layout_authentication_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./authentication-layout/authentication-layout.component */
    "./src/app/authentication/authentication-layout/authentication-layout.component.ts");
    /* harmony import */


    var _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/login/login.component */
    "./src/app/authentication/components/login/login.component.ts");
    /* harmony import */


    var _components_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/forgot-password/forgot-password.component */
    "./src/app/authentication/components/forgot-password/forgot-password.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _components_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./components/change-password/change-password.component */
    "./src/app/authentication/components/change-password/change-password.component.ts");
    /* harmony import */


    var _components_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./components/reset-password/reset-password.component */
    "./src/app/authentication/components/reset-password/reset-password.component.ts");
    /* harmony import */


    var ng_otp_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ng-otp-input */
    "./node_modules/ng-otp-input/fesm2015/ng-otp-input.js");

    var AuthenticationModule = function AuthenticationModule() {
      _classCallCheck(this, AuthenticationModule);
    };

    AuthenticationModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_authentication_layout_authentication_layout_component__WEBPACK_IMPORTED_MODULE_4__["AuthenticationLayoutComponent"], _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"], _components_forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__["ForgotPasswordComponent"], _components_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_8__["ChangePasswordComponent"], _components_reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_9__["ResetPasswordComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _authentication_routing_module__WEBPACK_IMPORTED_MODULE_3__["AuthenticationRoutingModule"], ng_otp_input__WEBPACK_IMPORTED_MODULE_10__["NgOtpInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"]]
    })], AuthenticationModule);
    /***/
  },

  /***/
  "./src/app/authentication/components/change-password/change-password.component.css":
  /*!*****************************************************************************************!*\
    !*** ./src/app/authentication/components/change-password/change-password.component.css ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthenticationComponentsChangePasswordChangePasswordComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGhlbnRpY2F0aW9uL2NvbXBvbmVudHMvY2hhbmdlLXBhc3N3b3JkL2NoYW5nZS1wYXNzd29yZC5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/authentication/components/change-password/change-password.component.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/authentication/components/change-password/change-password.component.ts ***!
    \****************************************************************************************/

  /*! exports provided: ChangePasswordComponent */

  /***/
  function srcAppAuthenticationComponentsChangePasswordChangePasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChangePasswordComponent", function () {
      return ChangePasswordComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ng6-toastr-notifications */
    "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
    /* harmony import */


    var src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/auth-service.service */
    "./src/app/services/auth-service.service.ts");
    /* harmony import */


    var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/utility.service */
    "./src/app/services/utility.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ChangePasswordComponent = /*#__PURE__*/function () {
      function ChangePasswordComponent(fb, authService, toastr, router, utility) {
        _classCallCheck(this, ChangePasswordComponent);

        this.fb = fb;
        this.authService = authService;
        this.toastr = toastr;
        this.router = router;
        this.utility = utility;
        this.passwordNotMatch = false;
      }

      _createClass(ChangePasswordComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initchangePasswordForm();
        }
      }, {
        key: "initchangePasswordForm",
        value: function initchangePasswordForm() {
          this.changePasswordForm = this.fb.group({
            old_password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            cpassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]]
          });
        }
      }, {
        key: "formSubmit",
        value: function formSubmit() {
          var _this4 = this;

          if (this.changePasswordForm.valid) {
            if (!this.utility.passwordMatchValidator(this.changePasswordForm)) {
              this.passwordNotMatch = false;
              var requestPayload = {
                email: this.authService.currentUserValue.email,
                old_password: this.changePasswordForm.value.old_password,
                new_password: this.changePasswordForm.value.password
              };
              this.authService.resetPasswordService(requestPayload).subscribe(function (response) {
                _this4.toastr.successToastr(response.body.message);

                _this4.router.navigate(['/home']);
              }, function (error) {
                _this4.toastr.errorToastr(error.error.message);

                _this4.router.navigate(['/authentication/login']);
              });
            } else {
              this.passwordNotMatch = true;
            }
          } else {
            this.utility.validateAllFormFields(this.changePasswordForm);
          }
        }
      }]);

      return ChangePasswordComponent;
    }();

    ChangePasswordComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_4__["AuthServiceService"]
      }, {
        type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__["ToastrManager"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__["UtilityService"]
      }];
    };

    ChangePasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-change-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./change-password.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/change-password/change-password.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./change-password.component.css */
      "./src/app/authentication/components/change-password/change-password.component.css"))["default"]]
    })], ChangePasswordComponent);
    /***/
  },

  /***/
  "./src/app/authentication/components/forgot-password/forgot-password.component.css":
  /*!*****************************************************************************************!*\
    !*** ./src/app/authentication/components/forgot-password/forgot-password.component.css ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthenticationComponentsForgotPasswordForgotPasswordComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".wrapper {\r\n    display: flex !important;\r\n    justify-content: space-between;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aGVudGljYXRpb24vY29tcG9uZW50cy9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx3QkFBd0I7SUFDeEIsOEJBQThCO0FBQ2xDIiwiZmlsZSI6InNyYy9hcHAvYXV0aGVudGljYXRpb24vY29tcG9uZW50cy9mb3Jnb3QtcGFzc3dvcmQvZm9yZ290LXBhc3N3b3JkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud3JhcHBlciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/authentication/components/forgot-password/forgot-password.component.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/authentication/components/forgot-password/forgot-password.component.ts ***!
    \****************************************************************************************/

  /*! exports provided: ForgotPasswordComponent */

  /***/
  function srcAppAuthenticationComponentsForgotPasswordForgotPasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotPasswordComponent", function () {
      return ForgotPasswordComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ng6-toastr-notifications */
    "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
    /* harmony import */


    var src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/auth-service.service */
    "./src/app/services/auth-service.service.ts");
    /* harmony import */


    var _services_utility_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../services/utility.service */
    "./src/app/services/utility.service.ts");

    var ForgotPasswordComponent = /*#__PURE__*/function () {
      function ForgotPasswordComponent(fb, authService, toastr, utility, router) {
        _classCallCheck(this, ForgotPasswordComponent);

        this.fb = fb;
        this.authService = authService;
        this.toastr = toastr;
        this.utility = utility;
        this.router = router;
        this.showOtpComponent = false;
        this.config = {
          allowNumbersOnly: true,
          length: 6,
          isPasswordInput: false,
          disableAutoFocus: false,
          placeholder: '',
          inputStyles: {
            width: '60px',
            height: '50px'
          }
        };
      }

      _createClass(ForgotPasswordComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initforgotForm();
        }
      }, {
        key: "onOtpChange",
        value: function onOtpChange(otp) {
          this.otp = otp;
        }
      }, {
        key: "initforgotForm",
        value: function initforgotForm() {
          this.forgotForm = this.fb.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]]
          });
        }
      }, {
        key: "formSubmit",
        value: function formSubmit() {
          var _this5 = this;

          if (this.forgotForm.valid) {
            this.authService.forgotService(this.forgotForm.value).subscribe(function (response) {
              _this5.toastr.successToastr(response.message);
            }, function (error) {
              _this5.toastr.errorToastr(error.error.message);
            });
          } else {
            this.utility.validateAllFormFields(this.forgotForm);
          }
        }
      }, {
        key: "formOtpSubmit",
        value: function formOtpSubmit() {
          var _this6 = this;

          console.log(this.otp);
          var requestData = {
            email: this.forgotForm.value.email
          };
          this.authService.otpVerify(requestData).subscribe(function (response) {
            _this6.toastr.successToastr(response.message);

            _this6.router.navigateByUrl('/authentication/reset-password', {
              state: requestData
            });
          }, function (error) {
            _this6.toastr.errorToastr(error.error.message);
          });
        }
      }]);

      return ForgotPasswordComponent;
    }();

    ForgotPasswordComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_5__["AuthServiceService"]
      }, {
        type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"]
      }, {
        type: _services_utility_service__WEBPACK_IMPORTED_MODULE_6__["UtilityService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('ngOtpInput', {
      "static": false
    })], ForgotPasswordComponent.prototype, "ngOtpInput", void 0);
    ForgotPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-forgot-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./forgot-password.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/forgot-password/forgot-password.component.html"))["default"],
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./forgot-password.component.css */
      "./src/app/authentication/components/forgot-password/forgot-password.component.css"))["default"]]
    })], ForgotPasswordComponent);
    /***/
  },

  /***/
  "./src/app/authentication/components/login/login.component.css":
  /*!*********************************************************************!*\
    !*** ./src/app/authentication/components/login/login.component.css ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthenticationComponentsLoginLoginComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGhlbnRpY2F0aW9uL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/authentication/components/login/login.component.ts":
  /*!********************************************************************!*\
    !*** ./src/app/authentication/components/login/login.component.ts ***!
    \********************************************************************/

  /*! exports provided: LoginComponent */

  /***/
  function srcAppAuthenticationComponentsLoginLoginComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginComponent", function () {
      return LoginComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _services_auth_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../services/auth-service.service */
    "./src/app/services/auth-service.service.ts");
    /* harmony import */


    var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ng6-toastr-notifications */
    "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
    /* harmony import */


    var _services_utility_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../services/utility.service */
    "./src/app/services/utility.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var angular_6_social_login__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-6-social-login */
    "./node_modules/angular-6-social-login/angular-6-social-login.umd.js");
    /* harmony import */


    var angular_6_social_login__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular_6_social_login__WEBPACK_IMPORTED_MODULE_8__);

    var LoginComponent = /*#__PURE__*/function () {
      function LoginComponent(fb, authService, socialAuthService, toastr, router, utility) {
        _classCallCheck(this, LoginComponent);

        this.fb = fb;
        this.authService = authService;
        this.socialAuthService = socialAuthService;
        this.toastr = toastr;
        this.router = router;
        this.utility = utility;
        this.loading = false;
        this.authorized = false;
      }

      _createClass(LoginComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initLoginForm();
        }
      }, {
        key: "initLoginForm",
        value: function initLoginForm() {
          this.loginForm = this.fb.group({
            email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email]],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(4)]]
          });
        }
      }, {
        key: "formSubmit",
        value: function formSubmit() {
          var _this7 = this;

          this.loading = true;

          if (this.loginForm.valid) {
            this.authService.loginService(this.loginForm.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["first"])()).subscribe(function (response) {
              _this7.loading = false;
              var responseData = response;

              if (!responseData.password_changed) {
                _this7.router.navigate(['/authentication/change-password']);

                return false;
              }

              _this7.router.navigate(['/home']);

              _this7.toastr.successToastr('Login successful');
            }, function (error) {
              _this7.loading = false;

              _this7.toastr.errorToastr(error);
            });
          } else {
            this.loading = false;
            this.utility.validateAllFormFields(this.loginForm);
          }
        }
      }, {
        key: "socialSignIn",
        value: function socialSignIn(socialPlatform) {
          var _this8 = this;

          var socialPlatformProvider;
          socialPlatformProvider = angular_6_social_login__WEBPACK_IMPORTED_MODULE_8__["GoogleLoginProvider"].PROVIDER_ID;
          this.socialAuthService.signIn(socialPlatformProvider).then(function (userData) {
            var reqParam = {
              email: userData.email
            };

            _this8.authService.socialLoginService(reqParam).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["first"])()).subscribe(function (response) {
              _this8.loading = false;

              _this8.router.navigate(['/home']);

              _this8.toastr.successToastr('Login successful');
            }, function (error) {
              _this8.loading = false;

              _this8.toastr.errorToastr(error);
            });
          }), function (error) {
            _this8.toastr.errorToastr('Invalid User');
          };
        }
      }]);

      return LoginComponent;
    }();

    LoginComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _services_auth_service_service__WEBPACK_IMPORTED_MODULE_3__["AuthServiceService"]
      }, {
        type: angular_6_social_login__WEBPACK_IMPORTED_MODULE_8__["AuthService"]
      }, {
        type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: _services_utility_service__WEBPACK_IMPORTED_MODULE_5__["UtilityService"]
      }];
    };

    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/login/login.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.component.css */
      "./src/app/authentication/components/login/login.component.css"))["default"]]
    })], LoginComponent);
    /***/
  },

  /***/
  "./src/app/authentication/components/reset-password/reset-password.component.css":
  /*!***************************************************************************************!*\
    !*** ./src/app/authentication/components/reset-password/reset-password.component.css ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppAuthenticationComponentsResetPasswordResetPasswordComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGhlbnRpY2F0aW9uL2NvbXBvbmVudHMvcmVzZXQtcGFzc3dvcmQvcmVzZXQtcGFzc3dvcmQuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/authentication/components/reset-password/reset-password.component.ts":
  /*!**************************************************************************************!*\
    !*** ./src/app/authentication/components/reset-password/reset-password.component.ts ***!
    \**************************************************************************************/

  /*! exports provided: ResetPasswordComponent */

  /***/
  function srcAppAuthenticationComponentsResetPasswordResetPasswordComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResetPasswordComponent", function () {
      return ResetPasswordComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ng6-toastr-notifications */
    "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
    /* harmony import */


    var src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/services/auth-service.service */
    "./src/app/services/auth-service.service.ts");
    /* harmony import */


    var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/services/utility.service */
    "./src/app/services/utility.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ResetPasswordComponent = /*#__PURE__*/function () {
      function ResetPasswordComponent(fb, authService, toastr, location, route, router, utility) {
        var _this9 = this;

        _classCallCheck(this, ResetPasswordComponent);

        this.fb = fb;
        this.authService = authService;
        this.toastr = toastr;
        this.location = location;
        this.route = route;
        this.router = router;
        this.utility = utility;
        this.passwordNotMatch = false;
        this.route.queryParams.subscribe(function (params) {
          _this9.userTokenForChangePassword = params.token;
        });
      }

      _createClass(ResetPasswordComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.initresetPasswordForm();
        }
      }, {
        key: "initresetPasswordForm",
        value: function initresetPasswordForm() {
          this.resetPasswordForm = this.fb.group({
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            cpassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]]
          });
        }
      }, {
        key: "formSubmit",
        value: function formSubmit() {
          var _this10 = this;

          if (this.resetPasswordForm.valid) {
            if (!this.utility.passwordMatchValidator(this.resetPasswordForm)) {
              this.passwordNotMatch = false;
              var requestPayload = {
                password: this.resetPasswordForm.value.password,
                token: this.userTokenForChangePassword
              };
              this.authService.changePasswordService(requestPayload).subscribe(function (response) {
                _this10.toastr.successToastr(response.body.message);

                _this10.router.navigateByUrl('/authentication/login');
              }, function (error) {//  this.toastr.errorToastr(error);
              });
            } else {
              this.passwordNotMatch = true;
            }
          } else {
            this.utility.validateAllFormFields(this.resetPasswordForm);
          }
        }
      }]);

      return ResetPasswordComponent;
    }();

    ResetPasswordComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: src_app_services_auth_service_service__WEBPACK_IMPORTED_MODULE_4__["AuthServiceService"]
      }, {
        type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_3__["ToastrManager"]
      }, {
        type: _angular_common__WEBPACK_IMPORTED_MODULE_6__["Location"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
      }, {
        type: src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__["UtilityService"]
      }];
    };

    ResetPasswordComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-reset-password',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./reset-password.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/authentication/components/reset-password/reset-password.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./reset-password.component.css */
      "./src/app/authentication/components/reset-password/reset-password.component.css"))["default"]]
    })], ResetPasswordComponent);
    /***/
  }
}]);
//# sourceMappingURL=authentication-authentication-module-es5.js.map