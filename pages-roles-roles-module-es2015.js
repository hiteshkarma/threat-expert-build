(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-roles-roles-module"],{

/***/ "./node_modules/primeng/components/tree/tree.js":
/*!******************************************************!*\
  !*** ./node_modules/primeng/components/tree/tree.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var core_2 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
var shared_1 = __webpack_require__(/*! ../common/shared */ "./node_modules/primeng/components/common/shared.js");
var shared_2 = __webpack_require__(/*! ../common/shared */ "./node_modules/primeng/components/common/shared.js");
var treedragdropservice_1 = __webpack_require__(/*! ../common/treedragdropservice */ "./node_modules/primeng/components/common/treedragdropservice.js");
var objectutils_1 = __webpack_require__(/*! ../utils/objectutils */ "./node_modules/primeng/components/utils/objectutils.js");
var api_1 = __webpack_require__(/*! ../common/api */ "./node_modules/primeng/components/common/api.js");
var UITreeNode = /** @class */ (function () {
    function UITreeNode(tree) {
        this.tree = tree;
    }
    UITreeNode_1 = UITreeNode;
    UITreeNode.prototype.ngOnInit = function () {
        this.node.parent = this.parentNode;
        if (this.parentNode) {
            this.tree.syncNodeOption(this.node, this.tree.value, 'parent', this.tree.getNodeWithKey(this.parentNode.key, this.tree.value));
        }
    };
    UITreeNode.prototype.getIcon = function () {
        var icon;
        if (this.node.icon)
            icon = this.node.icon;
        else
            icon = this.node.expanded && this.node.children && this.node.children.length ? this.node.expandedIcon : this.node.collapsedIcon;
        return UITreeNode_1.ICON_CLASS + ' ' + icon;
    };
    UITreeNode.prototype.isLeaf = function () {
        return this.tree.isNodeLeaf(this.node);
    };
    UITreeNode.prototype.toggle = function (event) {
        if (this.node.expanded)
            this.collapse(event);
        else
            this.expand(event);
    };
    UITreeNode.prototype.expand = function (event) {
        this.node.expanded = true;
        this.tree.onNodeExpand.emit({ originalEvent: event, node: this.node });
    };
    UITreeNode.prototype.collapse = function (event) {
        this.node.expanded = false;
        this.tree.onNodeCollapse.emit({ originalEvent: event, node: this.node });
    };
    UITreeNode.prototype.onNodeClick = function (event) {
        this.tree.onNodeClick(event, this.node);
    };
    UITreeNode.prototype.onNodeTouchEnd = function () {
        this.tree.onNodeTouchEnd();
    };
    UITreeNode.prototype.onNodeRightClick = function (event) {
        this.tree.onNodeRightClick(event, this.node);
    };
    UITreeNode.prototype.isSelected = function () {
        return this.tree.isSelected(this.node);
    };
    UITreeNode.prototype.onDropPoint = function (event, position) {
        var _this = this;
        event.preventDefault();
        var dragNode = this.tree.dragNode;
        var dragNodeIndex = this.tree.dragNodeIndex;
        var dragNodeScope = this.tree.dragNodeScope;
        var isValidDropPointIndex = this.tree.dragNodeTree === this.tree ? (position === 1 || dragNodeIndex !== this.index - 1) : true;
        if (this.tree.allowDrop(dragNode, this.node, dragNodeScope) && isValidDropPointIndex) {
            if (this.tree.validateDrop) {
                this.tree.onNodeDrop.emit({
                    originalEvent: event,
                    dragNode: dragNode,
                    dropNode: this.node,
                    dropIndex: this.index,
                    accept: function () {
                        _this.processPointDrop(dragNode, dragNodeIndex, position);
                    }
                });
            }
            else {
                this.processPointDrop(dragNode, dragNodeIndex, position);
                this.tree.onNodeDrop.emit({
                    originalEvent: event,
                    dragNode: dragNode,
                    dropNode: this.node,
                    dropIndex: this.index
                });
            }
        }
        this.draghoverPrev = false;
        this.draghoverNext = false;
    };
    UITreeNode.prototype.processPointDrop = function (dragNode, dragNodeIndex, position) {
        var newNodeList = this.node.parent ? this.node.parent.children : this.tree.value;
        this.tree.dragNodeSubNodes.splice(dragNodeIndex, 1);
        var dropIndex = this.index;
        if (position < 0) {
            dropIndex = (this.tree.dragNodeSubNodes === newNodeList) ? ((this.tree.dragNodeIndex > this.index) ? this.index : this.index - 1) : this.index;
            newNodeList.splice(dropIndex, 0, dragNode);
        }
        else {
            dropIndex = newNodeList.length;
            newNodeList.push(dragNode);
        }
        this.tree.dragDropService.stopDrag({
            node: dragNode,
            subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
            index: dragNodeIndex
        });
    };
    UITreeNode.prototype.onDropPointDragOver = function (event) {
        event.dataTransfer.dropEffect = 'move';
        event.preventDefault();
    };
    UITreeNode.prototype.onDropPointDragEnter = function (event, position) {
        if (this.tree.allowDrop(this.tree.dragNode, this.node, this.tree.dragNodeScope)) {
            if (position < 0)
                this.draghoverPrev = true;
            else
                this.draghoverNext = true;
        }
    };
    UITreeNode.prototype.onDropPointDragLeave = function (event) {
        this.draghoverPrev = false;
        this.draghoverNext = false;
    };
    UITreeNode.prototype.onDragStart = function (event) {
        if (this.tree.draggableNodes && this.node.draggable !== false) {
            event.dataTransfer.setData("text", "data");
            this.tree.dragDropService.startDrag({
                tree: this,
                node: this.node,
                subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
                index: this.index,
                scope: this.tree.draggableScope
            });
        }
        else {
            event.preventDefault();
        }
    };
    UITreeNode.prototype.onDragStop = function (event) {
        this.tree.dragDropService.stopDrag({
            node: this.node,
            subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
            index: this.index
        });
    };
    UITreeNode.prototype.onDropNodeDragOver = function (event) {
        event.dataTransfer.dropEffect = 'move';
        if (this.tree.droppableNodes) {
            event.preventDefault();
            event.stopPropagation();
        }
    };
    UITreeNode.prototype.onDropNode = function (event) {
        var _this = this;
        if (this.tree.droppableNodes && this.node.droppable !== false) {
            event.preventDefault();
            event.stopPropagation();
            var dragNode_1 = this.tree.dragNode;
            if (this.tree.allowDrop(dragNode_1, this.node, this.tree.dragNodeScope)) {
                if (this.tree.validateDrop) {
                    this.tree.onNodeDrop.emit({
                        originalEvent: event,
                        dragNode: dragNode_1,
                        dropNode: this.node,
                        index: this.index,
                        accept: function () {
                            _this.processNodeDrop(dragNode_1);
                        }
                    });
                }
                else {
                    this.processNodeDrop(dragNode_1);
                    this.tree.onNodeDrop.emit({
                        originalEvent: event,
                        dragNode: dragNode_1,
                        dropNode: this.node,
                        index: this.index
                    });
                }
            }
        }
        this.draghoverNode = false;
    };
    UITreeNode.prototype.processNodeDrop = function (dragNode) {
        var dragNodeIndex = this.tree.dragNodeIndex;
        this.tree.dragNodeSubNodes.splice(dragNodeIndex, 1);
        if (this.node.children)
            this.node.children.push(dragNode);
        else
            this.node.children = [dragNode];
        this.tree.dragDropService.stopDrag({
            node: dragNode,
            subNodes: this.node.parent ? this.node.parent.children : this.tree.value,
            index: this.tree.dragNodeIndex
        });
    };
    UITreeNode.prototype.onDropNodeDragEnter = function (event) {
        if (this.tree.droppableNodes && this.node.droppable !== false && this.tree.allowDrop(this.tree.dragNode, this.node, this.tree.dragNodeScope)) {
            this.draghoverNode = true;
        }
    };
    UITreeNode.prototype.onDropNodeDragLeave = function (event) {
        if (this.tree.droppableNodes) {
            var rect = event.currentTarget.getBoundingClientRect();
            if (event.x > rect.left + rect.width || event.x < rect.left || event.y >= Math.floor(rect.top + rect.height) || event.y < rect.top) {
                this.draghoverNode = false;
            }
        }
    };
    UITreeNode.prototype.onKeyDown = function (event) {
        var nodeElement = event.target.parentElement.parentElement;
        if (nodeElement.nodeName !== 'P-TREENODE') {
            return;
        }
        switch (event.which) {
            //down arrow
            case 40:
                var listElement = (this.tree.droppableNodes) ? nodeElement.children[1].children[1] : nodeElement.children[0].children[1];
                if (listElement && listElement.children.length > 0) {
                    this.focusNode(listElement.children[0]);
                }
                else {
                    var nextNodeElement = nodeElement.nextElementSibling;
                    if (nextNodeElement) {
                        this.focusNode(nextNodeElement);
                    }
                    else {
                        var nextSiblingAncestor = this.findNextSiblingOfAncestor(nodeElement);
                        if (nextSiblingAncestor) {
                            this.focusNode(nextSiblingAncestor);
                        }
                    }
                }
                event.preventDefault();
                break;
            //up arrow
            case 38:
                if (nodeElement.previousElementSibling) {
                    this.focusNode(this.findLastVisibleDescendant(nodeElement.previousElementSibling));
                }
                else {
                    var parentNodeElement = this.getParentNodeElement(nodeElement);
                    if (parentNodeElement) {
                        this.focusNode(parentNodeElement);
                    }
                }
                event.preventDefault();
                break;
            //right arrow
            case 39:
                if (!this.node.expanded) {
                    this.expand(event);
                }
                event.preventDefault();
                break;
            //left arrow
            case 37:
                if (this.node.expanded) {
                    this.collapse(event);
                }
                else {
                    var parentNodeElement = this.getParentNodeElement(nodeElement);
                    if (parentNodeElement) {
                        this.focusNode(parentNodeElement);
                    }
                }
                event.preventDefault();
                break;
            //enter
            case 13:
                this.tree.onNodeClick(event, this.node);
                event.preventDefault();
                break;
            default:
                //no op
                break;
        }
    };
    UITreeNode.prototype.findNextSiblingOfAncestor = function (nodeElement) {
        var parentNodeElement = this.getParentNodeElement(nodeElement);
        if (parentNodeElement) {
            if (parentNodeElement.nextElementSibling)
                return parentNodeElement.nextElementSibling;
            else
                return this.findNextSiblingOfAncestor(parentNodeElement);
        }
        else {
            return null;
        }
    };
    UITreeNode.prototype.findLastVisibleDescendant = function (nodeElement) {
        var childrenListElement = nodeElement.children[0].children[1];
        if (childrenListElement && childrenListElement.children.length > 0) {
            var lastChildElement = childrenListElement.children[childrenListElement.children.length - 1];
            return this.findLastVisibleDescendant(lastChildElement);
        }
        else {
            return nodeElement;
        }
    };
    UITreeNode.prototype.getParentNodeElement = function (nodeElement) {
        var parentNodeElement = nodeElement.parentElement.parentElement.parentElement;
        return parentNodeElement.tagName === 'P-TREENODE' ? parentNodeElement : null;
    };
    UITreeNode.prototype.focusNode = function (element) {
        if (this.tree.droppableNodes)
            element.children[1].children[0].focus();
        else
            element.children[0].children[0].focus();
    };
    var UITreeNode_1;
    UITreeNode.ICON_CLASS = 'ui-treenode-icon ';
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], UITreeNode.prototype, "node", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], UITreeNode.prototype, "parentNode", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], UITreeNode.prototype, "root", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], UITreeNode.prototype, "index", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], UITreeNode.prototype, "firstChild", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], UITreeNode.prototype, "lastChild", void 0);
    UITreeNode = UITreeNode_1 = __decorate([
        core_1.Component({
            selector: 'p-treeNode',
            template: "\n        <ng-template [ngIf]=\"node\">\n            <li *ngIf=\"tree.droppableNodes\" class=\"ui-treenode-droppoint\" [ngClass]=\"{'ui-treenode-droppoint-active ui-state-highlight':draghoverPrev}\"\n            (drop)=\"onDropPoint($event,-1)\" (dragover)=\"onDropPointDragOver($event)\" (dragenter)=\"onDropPointDragEnter($event,-1)\" (dragleave)=\"onDropPointDragLeave($event)\"></li>\n            <li *ngIf=\"!tree.horizontal\" role=\"treeitem\" [ngClass]=\"['ui-treenode',node.styleClass||'', isLeaf() ? 'ui-treenode-leaf': '']\">\n                <div class=\"ui-treenode-content\" (click)=\"onNodeClick($event)\" (contextmenu)=\"onNodeRightClick($event)\" (touchend)=\"onNodeTouchEnd()\"\n                    (drop)=\"onDropNode($event)\" (dragover)=\"onDropNodeDragOver($event)\" (dragenter)=\"onDropNodeDragEnter($event)\" (dragleave)=\"onDropNodeDragLeave($event)\"\n                    [draggable]=\"tree.draggableNodes\" (dragstart)=\"onDragStart($event)\" (dragend)=\"onDragStop($event)\" tabIndex=\"0\"\n                    [ngClass]=\"{'ui-treenode-selectable':tree.selectionMode && node.selectable !== false,'ui-treenode-dragover':draghoverNode, 'ui-treenode-content-selected':isSelected()}\" \n                    (keydown)=\"onKeyDown($event)\" [attr.aria-posinset]=\"this.index + 1\" [attr.aria-expanded]=\"this.node.expanded\" [attr.aria-selected]=\"isSelected()\">\n                    <span class=\"ui-tree-toggler pi pi-fw ui-unselectable-text\" [ngClass]=\"{'pi-caret-right':!node.expanded,'pi-caret-down':node.expanded}\"\n                            (click)=\"toggle($event)\"></span\n                    ><div class=\"ui-chkbox\" *ngIf=\"tree.selectionMode == 'checkbox'\"><div class=\"ui-chkbox-box ui-widget ui-corner-all ui-state-default\" [ngClass]=\"{'ui-state-disabled': node.selectable === false}\">\n                        <span class=\"ui-chkbox-icon ui-clickable pi\"\n                            [ngClass]=\"{'pi-check':isSelected(),'pi-minus':node.partialSelected}\"></span></div></div\n                    ><span [class]=\"getIcon()\" *ngIf=\"node.icon||node.expandedIcon||node.collapsedIcon\"></span\n                    ><span class=\"ui-treenode-label ui-corner-all\"\n                        [ngClass]=\"{'ui-state-highlight':isSelected()}\">\n                            <span *ngIf=\"!tree.getTemplateForNode(node)\">{{node.label}}</span>\n                            <span *ngIf=\"tree.getTemplateForNode(node)\">\n                                <ng-container *ngTemplateOutlet=\"tree.getTemplateForNode(node); context: {$implicit: node}\"></ng-container>\n                            </span>\n                    </span>\n                </div>\n                <ul class=\"ui-treenode-children\" style=\"display: none;\" *ngIf=\"node.children && node.expanded\" [style.display]=\"node.expanded ? 'block' : 'none'\" role=\"group\">\n                    <p-treeNode *ngFor=\"let childNode of node.children;let firstChild=first;let lastChild=last; let index=index; trackBy: tree.nodeTrackBy\" [node]=\"childNode\" [parentNode]=\"node\"\n                        [firstChild]=\"firstChild\" [lastChild]=\"lastChild\" [index]=\"index\"></p-treeNode>\n                </ul>\n            </li>\n            <li *ngIf=\"tree.droppableNodes&&lastChild\" class=\"ui-treenode-droppoint\" [ngClass]=\"{'ui-treenode-droppoint-active ui-state-highlight':draghoverNext}\"\n            (drop)=\"onDropPoint($event,1)\" (dragover)=\"onDropPointDragOver($event)\" (dragenter)=\"onDropPointDragEnter($event,1)\" (dragleave)=\"onDropPointDragLeave($event)\"></li>\n            <table *ngIf=\"tree.horizontal\" [class]=\"node.styleClass\">\n                <tbody>\n                    <tr>\n                        <td class=\"ui-treenode-connector\" *ngIf=\"!root\">\n                            <table class=\"ui-treenode-connector-table\">\n                                <tbody>\n                                    <tr>\n                                        <td [ngClass]=\"{'ui-treenode-connector-line':!firstChild}\"></td>\n                                    </tr>\n                                    <tr>\n                                        <td [ngClass]=\"{'ui-treenode-connector-line':!lastChild}\"></td>\n                                    </tr>\n                                </tbody>\n                            </table>\n                        </td>\n                        <td class=\"ui-treenode\" [ngClass]=\"{'ui-treenode-collapsed':!node.expanded}\">\n                            <div class=\"ui-treenode-content ui-state-default ui-corner-all\"\n                                [ngClass]=\"{'ui-treenode-selectable':tree.selectionMode,'ui-state-highlight':isSelected()}\" (click)=\"onNodeClick($event)\" (contextmenu)=\"onNodeRightClick($event)\"\n                                (touchend)=\"onNodeTouchEnd()\">\n                                <span class=\"ui-tree-toggler pi pi-fw ui-unselectable-text\" [ngClass]=\"{'pi-plus':!node.expanded,'pi-minus':node.expanded}\" *ngIf=\"!isLeaf()\"\n                                        (click)=\"toggle($event)\"></span\n                                ><span [class]=\"getIcon()\" *ngIf=\"node.icon||node.expandedIcon||node.collapsedIcon\"></span\n                                ><span class=\"ui-treenode-label ui-corner-all\">\n                                        <span *ngIf=\"!tree.getTemplateForNode(node)\">{{node.label}}</span>\n                                        <span *ngIf=\"tree.getTemplateForNode(node)\">\n                                        <ng-container *ngTemplateOutlet=\"tree.getTemplateForNode(node); context: {$implicit: node}\"></ng-container>\n                                        </span>\n                                </span>\n                            </div>\n                        </td>\n                        <td class=\"ui-treenode-children-container\" *ngIf=\"node.children && node.expanded\" [style.display]=\"node.expanded ? 'table-cell' : 'none'\">\n                            <div class=\"ui-treenode-children\">\n                                <p-treeNode *ngFor=\"let childNode of node.children;let firstChild=first;let lastChild=last; trackBy: tree.nodeTrackBy\" [node]=\"childNode\"\n                                        [firstChild]=\"firstChild\" [lastChild]=\"lastChild\"></p-treeNode>\n                            </div>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </ng-template>\n    "
        }),
        __param(0, core_1.Inject(core_1.forwardRef(function () { return Tree; }))),
        __metadata("design:paramtypes", [Object])
    ], UITreeNode);
    return UITreeNode;
}());
exports.UITreeNode = UITreeNode;
var Tree = /** @class */ (function () {
    function Tree(el, dragDropService) {
        this.el = el;
        this.dragDropService = dragDropService;
        this.selectionChange = new core_1.EventEmitter();
        this.onNodeSelect = new core_1.EventEmitter();
        this.onNodeUnselect = new core_1.EventEmitter();
        this.onNodeExpand = new core_1.EventEmitter();
        this.onNodeCollapse = new core_1.EventEmitter();
        this.onNodeContextMenuSelect = new core_1.EventEmitter();
        this.onNodeDrop = new core_1.EventEmitter();
        this.layout = 'vertical';
        this.metaKeySelection = true;
        this.propagateSelectionUp = true;
        this.propagateSelectionDown = true;
        this.loadingIcon = 'pi pi-spinner';
        this.emptyMessage = 'No records found';
        this.filterBy = 'label';
        this.filterMode = 'lenient';
        this.nodeTrackBy = function (index, item) { return item; };
    }
    Tree.prototype.ngOnInit = function () {
        var _this = this;
        if (this.droppableNodes) {
            this.dragStartSubscription = this.dragDropService.dragStart$.subscribe(function (event) {
                _this.dragNodeTree = event.tree;
                _this.dragNode = event.node;
                _this.dragNodeSubNodes = event.subNodes;
                _this.dragNodeIndex = event.index;
                _this.dragNodeScope = event.scope;
            });
            this.dragStopSubscription = this.dragDropService.dragStop$.subscribe(function (event) {
                _this.dragNodeTree = null;
                _this.dragNode = null;
                _this.dragNodeSubNodes = null;
                _this.dragNodeIndex = null;
                _this.dragNodeScope = null;
                _this.dragHover = false;
            });
        }
    };
    Object.defineProperty(Tree.prototype, "horizontal", {
        get: function () {
            return this.layout == 'horizontal';
        },
        enumerable: true,
        configurable: true
    });
    Tree.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.templates.length) {
            this.templateMap = {};
        }
        this.templates.forEach(function (item) {
            _this.templateMap[item.name] = item.template;
        });
    };
    Tree.prototype.onNodeClick = function (event, node) {
        var eventTarget = event.target;
        if (api_1.DomHandler.hasClass(eventTarget, 'ui-tree-toggler')) {
            return;
        }
        else if (this.selectionMode) {
            if (node.selectable === false) {
                return;
            }
            if (this.hasFilteredNodes()) {
                node = this.getNodeWithKey(node.key, this.value);
                if (!node) {
                    return;
                }
            }
            var index_1 = this.findIndexInSelection(node);
            var selected = (index_1 >= 0);
            if (this.isCheckboxSelectionMode()) {
                if (selected) {
                    if (this.propagateSelectionDown)
                        this.propagateDown(node, false);
                    else
                        this.selection = this.selection.filter(function (val, i) { return i != index_1; });
                    if (this.propagateSelectionUp && node.parent) {
                        this.propagateUp(node.parent, false);
                    }
                    this.selectionChange.emit(this.selection);
                    this.onNodeUnselect.emit({ originalEvent: event, node: node });
                }
                else {
                    if (this.propagateSelectionDown)
                        this.propagateDown(node, true);
                    else
                        this.selection = (this.selection || []).concat([node]);
                    if (this.propagateSelectionUp && node.parent) {
                        this.propagateUp(node.parent, true);
                    }
                    this.selectionChange.emit(this.selection);
                    this.onNodeSelect.emit({ originalEvent: event, node: node });
                }
            }
            else {
                var metaSelection = this.nodeTouched ? false : this.metaKeySelection;
                if (metaSelection) {
                    var metaKey = (event.metaKey || event.ctrlKey);
                    if (selected && metaKey) {
                        if (this.isSingleSelectionMode()) {
                            this.selectionChange.emit(null);
                        }
                        else {
                            this.selection = this.selection.filter(function (val, i) { return i != index_1; });
                            this.selectionChange.emit(this.selection);
                        }
                        this.onNodeUnselect.emit({ originalEvent: event, node: node });
                    }
                    else {
                        if (this.isSingleSelectionMode()) {
                            this.selectionChange.emit(node);
                        }
                        else if (this.isMultipleSelectionMode()) {
                            this.selection = (!metaKey) ? [] : this.selection || [];
                            this.selection = this.selection.concat([node]);
                            this.selectionChange.emit(this.selection);
                        }
                        this.onNodeSelect.emit({ originalEvent: event, node: node });
                    }
                }
                else {
                    if (this.isSingleSelectionMode()) {
                        if (selected) {
                            this.selection = null;
                            this.onNodeUnselect.emit({ originalEvent: event, node: node });
                        }
                        else {
                            this.selection = node;
                            this.onNodeSelect.emit({ originalEvent: event, node: node });
                        }
                    }
                    else {
                        if (selected) {
                            this.selection = this.selection.filter(function (val, i) { return i != index_1; });
                            this.onNodeUnselect.emit({ originalEvent: event, node: node });
                        }
                        else {
                            this.selection = (this.selection || []).concat([node]);
                            this.onNodeSelect.emit({ originalEvent: event, node: node });
                        }
                    }
                    this.selectionChange.emit(this.selection);
                }
            }
        }
        this.nodeTouched = false;
    };
    Tree.prototype.onNodeTouchEnd = function () {
        this.nodeTouched = true;
    };
    Tree.prototype.onNodeRightClick = function (event, node) {
        if (this.contextMenu) {
            var eventTarget = event.target;
            if (eventTarget.className && eventTarget.className.indexOf('ui-tree-toggler') === 0) {
                return;
            }
            else {
                var index = this.findIndexInSelection(node);
                var selected = (index >= 0);
                if (!selected) {
                    if (this.isSingleSelectionMode())
                        this.selectionChange.emit(node);
                    else
                        this.selectionChange.emit([node]);
                }
                this.contextMenu.show(event);
                this.onNodeContextMenuSelect.emit({ originalEvent: event, node: node });
            }
        }
    };
    Tree.prototype.findIndexInSelection = function (node) {
        var index = -1;
        if (this.selectionMode && this.selection) {
            if (this.isSingleSelectionMode()) {
                var areNodesEqual = (this.selection.key && this.selection.key === node.key) || this.selection == node;
                index = areNodesEqual ? 0 : -1;
            }
            else {
                for (var i = 0; i < this.selection.length; i++) {
                    var selectedNode = this.selection[i];
                    var areNodesEqual = (selectedNode.key && selectedNode.key === node.key) || selectedNode == node;
                    if (areNodesEqual) {
                        index = i;
                        break;
                    }
                }
            }
        }
        return index;
    };
    Tree.prototype.syncNodeOption = function (node, parentNodes, option, value) {
        // to synchronize the node option between the filtered nodes and the original nodes(this.value) 
        var _node = this.hasFilteredNodes() ? this.getNodeWithKey(node.key, parentNodes) : null;
        if (_node) {
            _node[option] = value || node[option];
        }
    };
    Tree.prototype.hasFilteredNodes = function () {
        return this.filter && this.filteredNodes && this.filteredNodes.length;
    };
    Tree.prototype.getNodeWithKey = function (key, nodes) {
        for (var _i = 0, nodes_1 = nodes; _i < nodes_1.length; _i++) {
            var node = nodes_1[_i];
            if (node.key === key) {
                return node;
            }
            if (node.children) {
                var matchedNode = this.getNodeWithKey(key, node.children);
                if (matchedNode) {
                    return matchedNode;
                }
            }
        }
    };
    Tree.prototype.propagateUp = function (node, select) {
        if (node.children && node.children.length) {
            var selectedCount = 0;
            var childPartialSelected = false;
            for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
                var child = _a[_i];
                if (this.isSelected(child)) {
                    selectedCount++;
                }
                else if (child.partialSelected) {
                    childPartialSelected = true;
                }
            }
            if (select && selectedCount == node.children.length) {
                this.selection = (this.selection || []).concat([node]);
                node.partialSelected = false;
            }
            else {
                if (!select) {
                    var index_2 = this.findIndexInSelection(node);
                    if (index_2 >= 0) {
                        this.selection = this.selection.filter(function (val, i) { return i != index_2; });
                    }
                }
                if (childPartialSelected || selectedCount > 0 && selectedCount != node.children.length)
                    node.partialSelected = true;
                else
                    node.partialSelected = false;
            }
            this.syncNodeOption(node, this.filteredNodes, 'partialSelected');
        }
        var parent = node.parent;
        if (parent) {
            this.propagateUp(parent, select);
        }
    };
    Tree.prototype.propagateDown = function (node, select) {
        var index = this.findIndexInSelection(node);
        if (select && index == -1) {
            this.selection = (this.selection || []).concat([node]);
        }
        else if (!select && index > -1) {
            this.selection = this.selection.filter(function (val, i) { return i != index; });
        }
        node.partialSelected = false;
        this.syncNodeOption(node, this.filteredNodes, 'partialSelected');
        if (node.children && node.children.length) {
            for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
                var child = _a[_i];
                this.propagateDown(child, select);
            }
        }
    };
    Tree.prototype.isSelected = function (node) {
        return this.findIndexInSelection(node) != -1;
    };
    Tree.prototype.isSingleSelectionMode = function () {
        return this.selectionMode && this.selectionMode == 'single';
    };
    Tree.prototype.isMultipleSelectionMode = function () {
        return this.selectionMode && this.selectionMode == 'multiple';
    };
    Tree.prototype.isCheckboxSelectionMode = function () {
        return this.selectionMode && this.selectionMode == 'checkbox';
    };
    Tree.prototype.isNodeLeaf = function (node) {
        return node.leaf == false ? false : !(node.children && node.children.length);
    };
    Tree.prototype.getRootNode = function () {
        return this.filteredNodes ? this.filteredNodes : this.value;
    };
    Tree.prototype.getTemplateForNode = function (node) {
        if (this.templateMap)
            return node.type ? this.templateMap[node.type] : this.templateMap['default'];
        else
            return null;
    };
    Tree.prototype.onDragOver = function (event) {
        if (this.droppableNodes && (!this.value || this.value.length === 0)) {
            event.dataTransfer.dropEffect = 'move';
            event.preventDefault();
        }
    };
    Tree.prototype.onDrop = function (event) {
        if (this.droppableNodes && (!this.value || this.value.length === 0)) {
            event.preventDefault();
            var dragNode = this.dragNode;
            if (this.allowDrop(dragNode, null, this.dragNodeScope)) {
                var dragNodeIndex = this.dragNodeIndex;
                this.dragNodeSubNodes.splice(dragNodeIndex, 1);
                this.value = this.value || [];
                this.value.push(dragNode);
                this.dragDropService.stopDrag({
                    node: dragNode
                });
            }
        }
    };
    Tree.prototype.onDragEnter = function (event) {
        if (this.droppableNodes && this.allowDrop(this.dragNode, null, this.dragNodeScope)) {
            this.dragHover = true;
        }
    };
    Tree.prototype.onDragLeave = function (event) {
        if (this.droppableNodes) {
            var rect = event.currentTarget.getBoundingClientRect();
            if (event.x > rect.left + rect.width || event.x < rect.left || event.y > rect.top + rect.height || event.y < rect.top) {
                this.dragHover = false;
            }
        }
    };
    Tree.prototype.allowDrop = function (dragNode, dropNode, dragNodeScope) {
        if (!dragNode) {
            //prevent random html elements to be dragged
            return false;
        }
        else if (this.isValidDragScope(dragNodeScope)) {
            var allow = true;
            if (dropNode) {
                if (dragNode === dropNode) {
                    allow = false;
                }
                else {
                    var parent_1 = dropNode.parent;
                    while (parent_1 != null) {
                        if (parent_1 === dragNode) {
                            allow = false;
                            break;
                        }
                        parent_1 = parent_1.parent;
                    }
                }
            }
            return allow;
        }
        else {
            return false;
        }
    };
    Tree.prototype.isValidDragScope = function (dragScope) {
        var dropScope = this.droppableScope;
        if (dropScope) {
            if (typeof dropScope === 'string') {
                if (typeof dragScope === 'string')
                    return dropScope === dragScope;
                else if (dragScope instanceof Array)
                    return dragScope.indexOf(dropScope) != -1;
            }
            else if (dropScope instanceof Array) {
                if (typeof dragScope === 'string') {
                    return dropScope.indexOf(dragScope) != -1;
                }
                else if (dragScope instanceof Array) {
                    for (var _i = 0, dropScope_1 = dropScope; _i < dropScope_1.length; _i++) {
                        var s = dropScope_1[_i];
                        for (var _a = 0, dragScope_1 = dragScope; _a < dragScope_1.length; _a++) {
                            var ds = dragScope_1[_a];
                            if (s === ds) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
        else {
            return true;
        }
    };
    Tree.prototype.onFilter = function (event) {
        var filterValue = event.target.value;
        if (filterValue === '') {
            this.filteredNodes = null;
        }
        else {
            this.filteredNodes = [];
            var searchFields = this.filterBy.split(',');
            var filterText = objectutils_1.ObjectUtils.removeAccents(filterValue).toLowerCase();
            var isStrictMode = this.filterMode === 'strict';
            for (var _i = 0, _a = this.value; _i < _a.length; _i++) {
                var node = _a[_i];
                var copyNode = __assign({}, node);
                var paramsWithoutNode = { searchFields: searchFields, filterText: filterText, isStrictMode: isStrictMode };
                if ((isStrictMode && (this.findFilteredNodes(copyNode, paramsWithoutNode) || this.isFilterMatched(copyNode, paramsWithoutNode))) ||
                    (!isStrictMode && (this.isFilterMatched(copyNode, paramsWithoutNode) || this.findFilteredNodes(copyNode, paramsWithoutNode)))) {
                    this.filteredNodes.push(copyNode);
                }
            }
        }
    };
    Tree.prototype.findFilteredNodes = function (node, paramsWithoutNode) {
        if (node) {
            var matched = false;
            if (node.children) {
                var childNodes = node.children.slice();
                node.children = [];
                for (var _i = 0, childNodes_1 = childNodes; _i < childNodes_1.length; _i++) {
                    var childNode = childNodes_1[_i];
                    var copyChildNode = __assign({}, childNode);
                    if (this.isFilterMatched(copyChildNode, paramsWithoutNode)) {
                        matched = true;
                        node.children.push(copyChildNode);
                    }
                }
            }
            if (matched) {
                node.expanded = true;
                return true;
            }
        }
    };
    Tree.prototype.isFilterMatched = function (node, _a) {
        var searchFields = _a.searchFields, filterText = _a.filterText, isStrictMode = _a.isStrictMode;
        var matched = false;
        for (var _i = 0, searchFields_1 = searchFields; _i < searchFields_1.length; _i++) {
            var field = searchFields_1[_i];
            var fieldValue = objectutils_1.ObjectUtils.removeAccents(String(objectutils_1.ObjectUtils.resolveFieldData(node, field))).toLowerCase();
            if (fieldValue.indexOf(filterText) > -1) {
                matched = true;
            }
        }
        if (!matched || (isStrictMode && !this.isNodeLeaf(node))) {
            matched = this.findFilteredNodes(node, { searchFields: searchFields, filterText: filterText, isStrictMode: isStrictMode }) || matched;
        }
        return matched;
    };
    Tree.prototype.getBlockableElement = function () {
        return this.el.nativeElement.children[0];
    };
    Tree.prototype.ngOnDestroy = function () {
        if (this.dragStartSubscription) {
            this.dragStartSubscription.unsubscribe();
        }
        if (this.dragStopSubscription) {
            this.dragStopSubscription.unsubscribe();
        }
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], Tree.prototype, "value", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "selectionMode", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tree.prototype, "selection", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "selectionChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeSelect", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeUnselect", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeExpand", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeCollapse", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeContextMenuSelect", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Tree.prototype, "onNodeDrop", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tree.prototype, "style", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "styleClass", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tree.prototype, "contextMenu", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "layout", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tree.prototype, "draggableScope", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Tree.prototype, "droppableScope", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "draggableNodes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "droppableNodes", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "metaKeySelection", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "propagateSelectionUp", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "propagateSelectionDown", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "loading", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "loadingIcon", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "emptyMessage", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "ariaLabel", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "ariaLabelledBy", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "validateDrop", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Tree.prototype, "filter", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "filterBy", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "filterMode", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Tree.prototype, "filterPlaceholder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Function)
    ], Tree.prototype, "nodeTrackBy", void 0);
    __decorate([
        core_1.ContentChildren(shared_2.PrimeTemplate),
        __metadata("design:type", core_1.QueryList)
    ], Tree.prototype, "templates", void 0);
    Tree = __decorate([
        core_1.Component({
            selector: 'p-tree',
            template: "\n        <div [ngClass]=\"{'ui-tree ui-widget ui-widget-content ui-corner-all':true,'ui-tree-selectable':selectionMode,'ui-treenode-dragover':dragHover,'ui-tree-loading': loading}\" [ngStyle]=\"style\" [class]=\"styleClass\" *ngIf=\"!horizontal\"\n            (drop)=\"onDrop($event)\" (dragover)=\"onDragOver($event)\" (dragenter)=\"onDragEnter($event)\" (dragleave)=\"onDragLeave($event)\">\n            <div class=\"ui-tree-loading-mask ui-widget-overlay\" *ngIf=\"loading\"></div>\n            <div class=\"ui-tree-loading-content\" *ngIf=\"loading\">\n                <i [class]=\"'ui-tree-loading-icon pi-spin ' + loadingIcon\"></i>\n            </div>\n            <div *ngIf=\"filter\" class=\"ui-tree-filter-container\">\n                <input #filter type=\"text\" autocomplete=\"off\" class=\"ui-tree-filter ui-inputtext ui-widget ui-state-default ui-corner-all\" [attr.placeholder]=\"filterPlaceholder\"\n                    (keydown.enter)=\"$event.preventDefault()\" (input)=\"onFilter($event)\">\n                    <span class=\"ui-tree-filter-icon pi pi-search\"></span>\n            </div>\n            <ul class=\"ui-tree-container\" *ngIf=\"getRootNode()\" role=\"tree\" [attr.aria-label]=\"ariaLabel\" [attr.aria-labelledby]=\"ariaLabelledBy\">\n                <p-treeNode *ngFor=\"let node of getRootNode(); let firstChild=first;let lastChild=last; let index=index; trackBy: nodeTrackBy\" [node]=\"node\"\n                [firstChild]=\"firstChild\" [lastChild]=\"lastChild\" [index]=\"index\"></p-treeNode>\n            </ul>\n            <div class=\"ui-tree-empty-message\" *ngIf=\"!loading && (value == null || value.length === 0)\">{{emptyMessage}}</div>\n        </div>\n        <div [ngClass]=\"{'ui-tree ui-tree-horizontal ui-widget ui-widget-content ui-corner-all':true,'ui-tree-selectable':selectionMode}\"  [ngStyle]=\"style\" [class]=\"styleClass\" *ngIf=\"horizontal\">\n            <div class=\"ui-tree-loading ui-widget-overlay\" *ngIf=\"loading\"></div>\n            <div class=\"ui-tree-loading-content\" *ngIf=\"loading\">\n                <i [class]=\"'ui-tree-loading-icon pi-spin ' + loadingIcon\"></i>\n            </div>\n            <table *ngIf=\"value&&value[0]\">\n                <p-treeNode [node]=\"value[0]\" [root]=\"true\"></p-treeNode>\n            </table>\n            <div class=\"ui-tree-empty-message\" *ngIf=\"!loading && (value == null || value.length === 0)\">{{emptyMessage}}</div>\n        </div>\n    "
        }),
        __param(1, core_2.Optional()),
        __metadata("design:paramtypes", [core_1.ElementRef, treedragdropservice_1.TreeDragDropService])
    ], Tree);
    return Tree;
}());
exports.Tree = Tree;
var TreeModule = /** @class */ (function () {
    function TreeModule() {
    }
    TreeModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            exports: [Tree, shared_1.SharedModule],
            declarations: [Tree, UITreeNode]
        })
    ], TreeModule);
    return TreeModule;
}());
exports.TreeModule = TreeModule;
//# sourceMappingURL=tree.js.map

/***/ }),

/***/ "./node_modules/primeng/tree.js":
/*!**************************************!*\
  !*** ./node_modules/primeng/tree.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* Shorthand */

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./components/tree/tree */ "./node_modules/primeng/components/tree/tree.js"));

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"py-4\">\r\n    <div class=\"pages-header\">\r\n        <h4>{{isUpdate ? 'Update' : 'New'}} Role</h4>\r\n        <div>\r\n            <a [routerLink]=\"[ '/home/roles']\" class=\"btn btn-secondary sbt-btn mr-3\">Cancel</a>\r\n            <a href=\"javascript:void(0)\" class=\"btn btn-primary sbt-btn\" (click)=\"formSubmit()\">{{isUpdate ? 'Update' : 'Save'}}</a>\r\n            <!--<button class=\"btn btn-default\" [disabled]=\"roleForm.invalid\">{{isUpdate ? 'Update' : 'Submit'}}</button>-->\r\n        </div>\r\n    </div>\r\n</section>\r\n<section class=\"add-user-form roles-form\">\r\n    <form [formGroup]=\"roleForm\">\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Role Name <span class=\"text-danger\">*</span></label>\r\n            <div>\r\n                <input type=\"text\" formControlName=\"title\" class=\"form-control\" style=\"max-height: 35px;\" placeholder=\"Role Name\" [class.is-invalid]=\"roleForm.controls.title.invalid && (roleForm.controls.title.dirty || roleForm.controls.title.touched)\">\r\n                <p class=\"error-msg\" *ngIf=\"roleForm.controls.title.invalid && (roleForm.controls.title.dirty || roleForm.controls.title.touched)\">Invalid role name</p>\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label class=\"control-label pl-1\" style=\"margin-top: 1px; font-weight: normal;\">Permissions</label>\r\n        </div>\r\n        <div class=\"checkbox-tabs\">\r\n            <div class=\"tab-wrapper\">\r\n                <p-tabMenu [model]=\"menu\" styleClass=\"tab-menu\" class=\"tab-menu\" [activeItem]=\"activeItem\">\r\n                </p-tabMenu>\r\n            </div>\r\n            <div class=\"form-group\">\r\n                <div class=\"checkbox pl-5\">\r\n                    <ng-container formArrayName=\"permissions\">\r\n                        <div class=\"mb-3\" *ngFor=\"let item of permission().controls; let i=index\" [formGroupName]=\"i\">\r\n                            <ng-container *ngIf=\"i+1 == activeTab\">\r\n                                <ng-container formArrayName=\"privilege\">\r\n                                    <div class=\"mb-3\" *ngFor=\"let sitem of item.controls.privilege.controls; let prev=index\" [formGroupName]=\"prev\">\r\n                                        <ng-container>\r\n                                            <div class=\"form-group\">\r\n                                                <input type=\"checkbox\" [checked]=\"sitem?.value?.checked\" [id]=\"sitem?.value?.title\" formControlName=\"checked\">\r\n                                                <label>{{sitem?.value?.title}}</label>\r\n                                            </div>\r\n                                            <ng-container formArrayName=\"subPrivilege\">\r\n                                                <div class=\"form-group pl-5\" *ngFor=\"let sub of sitem.controls.subPrivilege.controls; let subIndex=index\" [formGroupName]=\"subIndex\">\r\n                                                    <div class=\"d-flex\">\r\n                                                        <input type=\"checkbox\" [id]=\"sub?.value?.title\" [checked]=\"sub?.value?.checked && sitem?.value?.checked\" [attr.disabled]=\"!sitem?.value?.checked ? true : null\" formControlName=\"checked\">\r\n                                                        <label>{{sub?.value.title}}</label>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </ng-container>\r\n                                        </ng-container>\r\n                                    </div>\r\n                                </ng-container>\r\n                            </ng-container>\r\n\r\n                        </div>\r\n                    </ng-container>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!--<div class=\"text-center\">\r\n            <button class=\"btn btn-default\" [disabled]=\"roleForm.invalid\">{{isUpdate ? 'Update' : 'Submit'}}</button>\r\n        </div>-->\r\n    </form>\r\n</section>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.html":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"user-table-section\">\r\n    <div class=\"pages-header\">\r\n        <h4>Roles</h4>\r\n        <a [routerLink]=\"[ '/home/roles/add-role']\" class=\"btn btn-primary sbt-btn\" *ngIf=\"jwtService.IsParentChildPermission('View Role', 'Create Roles')\">New Role</a>\r\n    </div>\r\n    <div class=\"card table-main user-table-main\">\r\n        <div class=\"row\">\r\n            <div class=\"col-md-9\">\r\n                <div class=\"user-table mt-4\">\r\n                    <p-table [value]=\"usersList\" [paginator]=\"true\" [rows]=\"10\" [showCurrentPageReport]=\"true\" [rowsPerPageOptions]=\"[10,25,50]\">\r\n                        <ng-template pTemplate=\"header\">\r\n                            <tr>\r\n                                <th>S. No.</th>\r\n                                <th>Name</th>\r\n                                <th>Agents</th>\r\n                                <th></th>\r\n                            </tr>\r\n                        </ng-template>\r\n                        <ng-template pTemplate=\"body\" let-user let-i=\"rowIndex\">\r\n                            <tr (click)=\"getRoleDetails('/home/roles/edit-role', user?.id, user?.is_deletable)\" *ngIf=\"jwtService.IsParentChildPermission('View Role', 'Edit Role permission')\">\r\n                                <td>{{i + 1}}</td>\r\n                                <td>{{user?.title}}</td>\r\n                                <td>\r\n                                    <span class=\"cursor-pointer\" (click)=\"showAgents($event, i)\">\r\n                                        {{user?.agent_count ? user?.agent_count : 0}}\r\n                                    </span>\r\n                                <td>\r\n                                    <span class=\"delete text-danger\" *ngIf=\"user?.is_deletable && jwtService.IsParentChildPermission('View Role', 'Delete Roles')\" title=\"Delete Role\" (click)=\"deleteRole($event, user?.id)\">\r\n                                        <i class=\"pi pi-trash\"></i>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                            <tr *ngIf=\"!jwtService.IsParentChildPermission('View Role', 'Edit Role permission')\">\r\n                                <td>{{i + 1}}</td>\r\n                                <td>{{user?.title}}</td>\r\n                                <td>\r\n                                    <span class=\"cursor-pointer\" (click)=\"showAgents($event, i)\">\r\n                                        {{user?.agent_count ? user?.agent_count : 0}}\r\n                                    </span>\r\n                                <td>\r\n                                    <span class=\"delete text-danger\" *ngIf=\"user?.is_deletable && jwtService.IsParentChildPermission('View Role', 'Delete Roles')\" title=\"Delete Role\" (click)=\"deleteRole($event, user?.id)\">\r\n                                        <i class=\"pi pi-trash\"></i>\r\n                                    </span>\r\n                                </td>\r\n                            </tr>\r\n                        </ng-template>\r\n                    </p-table>\r\n                </div>\r\n            </div>\r\n            <div class=\"col-md-3\">\r\n                <div class=\"list-desc\">\r\n                    <h3>Roles</h3>\r\n                    <p class=\"mb-0\">The list shows all the Roles added in your help desk.</p>\r\n                    <p>You can add new role by clicking on \"New Role\" Button.</p>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n<p-confirmDialog header=\"Confirmation\" icon=\"pi pi-exclamation-triangle\"></p-confirmDialog>\r\n<!-- Modal -->\r\n<p-dialog header=\"Agents\" [(visible)]=\"isModalOpen\" [modal]=\"true\" [style]=\"{width: '50vw'}\" [focusOnShow]=\"true\" [baseZIndex]=\"10000\" [transitionOptions]=\"'200ms'\">\r\n    <div class=\"table-main modal-table\">\r\n        <p-table [value]=\"agents\">\r\n            <ng-template pTemplate=\"header\">\r\n                <tr>\r\n                    <th>S. No.</th>\r\n                    <th>User Name</th>\r\n                    <th>Email ID</th>\r\n                    <th></th>\r\n                </tr>\r\n            </ng-template>\r\n            <ng-template pTemplate=\"body\" let-agent let-i=\"rowIndex\">\r\n                <tr>\r\n                    <td>{{i + 1}}</td>\r\n                    <td>{{agent?.username}}</td>\r\n                    <td class=\"text-left\">\r\n                        {{agent?.email}}\r\n                    </td>\r\n                </tr>\r\n            </ng-template>\r\n        </p-table>\r\n    </div>\r\n    <p-footer>\r\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"closeModal()\">Close</button>\r\n    </p-footer>\r\n</p-dialog>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<section class=\"page-content\">\n    <router-outlet></router-outlet>\n</section>\n");

/***/ }),

/***/ "./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.css ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3JvbGVzL3BhZ2VzL2FkZC1yb2xlcy9hZGQtcm9sZXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.ts ***!
  \********************************************************************************/
/*! exports provided: AddRolesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRolesComponent", function() { return AddRolesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
/* harmony import */ var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/utility.service */ "./src/app/services/utility.service.ts");
/* harmony import */ var src_app_shared_nodeService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/shared/nodeService */ "./src/app/shared/nodeService.ts");
/* harmony import */ var _services_app_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../../services/app.service */ "./src/app/services/app.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");









let AddRolesComponent = class AddRolesComponent {
    constructor(fb, toastr, router, route, utility, changeDetection, service, nodeService) {
        this.fb = fb;
        this.toastr = toastr;
        this.router = router;
        this.route = route;
        this.utility = utility;
        this.changeDetection = changeDetection;
        this.service = service;
        this.nodeService = nodeService;
        this.isUpdate = false;
        this.allPermission = [];
        this.clearFormArray = (formArray) => {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        };
        this.route.params
            .pipe()
            .subscribe(param => {
            this.roleId = param.id;
            if (this.roleId) {
                this.isUpdate = true;
                this.getRoleDetails(this.roleId);
            }
        });
    }
    ngOnInit() {
        this.initRoleForm();
        if (!this.isUpdate) {
            this.getPermissions();
        }
        this.nodeService.getFiles().then(files => this.files3 = files);
        this.menu = [
            {
                label: 'Projects',
                id: '1',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[0];
                    //  this.createPermissionCheckbox(0);
                }
            },
            {
                label: 'Templates',
                id: '2',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[1];
                    //    this.createPermissionCheckbox(1);
                }
            },
            {
                label: 'Components',
                id: '3',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[2];
                    //   this.createPermissionCheckbox(2);
                }
            },
            {
                label: 'Threats',
                id: '4',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[3];
                    //    this.createPermissionCheckbox(3);
                }
            },
            {
                label: 'Tickets',
                id: '5',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[4];
                    //    this.createPermissionCheckbox(4);
                }
            },
            {
                label: 'Admin',
                id: '6',
                command: (event) => {
                    this.activeTab = event.item.id;
                    this.activeItem = this.menu[5];
                    //   this.createPermissionCheckbox(5);
                }
            }
        ];
        this.activeTab = '1';
        this.activeItem = this.menu[0];
        if (this.isUpdate) {
            this.roleForm.get('id').setValue(this.roleId);
        }
        else {
            // this.createPermissionCheckbox();
        }
    }
    getPermissions() {
        this.service.getPermissions().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["first"])())
            .subscribe(response => {
            this.allPermission = response;
            this.createPermissionCheckbox();
        }, error => {
            this.toastr.errorToastr(error.error.message);
        });
    }
    createPermissionCheckbox() {
        this.clearFormArray(this.permission());
        this.allPermission.forEach((element, k) => {
            this.addPermission();
            element.forEach((privilege, index) => {
                this.addPrev(k, privilege);
                if (privilege.SubPrivilege && privilege.SubPrivilege.length) {
                    privilege.SubPrivilege.forEach((el) => {
                        this.addSubPrev(k, index, el);
                    });
                }
            });
        });
        if (this.isUpdate) {
            //   this.getRoleDetails(this.roleId);
        }
    }
    initRoleForm() {
        this.roleForm = this.fb.group({
            id: [''],
            title: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
            permissions: this.fb.array([]),
        });
    }
    permission() {
        return this.roleForm.get('permissions');
    }
    newPermission() {
        return this.fb.group({
            privilege: this.fb.array([])
        });
    }
    addPermission() {
        this.permission().push(this.newPermission());
    }
    prev(index) {
        return this.permission().at(index).get('privilege');
    }
    newPrev(element) {
        return this.fb.group({
            title: [element.title],
            checked: [element.checked],
            subPrivilege: this.fb.array([])
        });
    }
    addPrev(quesIndex, element) {
        this.prev(quesIndex).push(this.newPrev(element));
    }
    // sub_prev(index): FormArray {
    //     return this.prev(index).get('sub_prev') as FormArray;
    // }
    sub_prev(ix, iy) {
        return this.permission().at(ix).get('privilege').at(iy).get('subPrivilege');
    }
    newSubPrev(element) {
        return this.fb.group({
            title: [element.title],
            checked: [element.checked],
        });
    }
    addSubPrev(prevIndex, quesIndex, element) {
        this.sub_prev(prevIndex, quesIndex).push(this.newSubPrev(element));
    }
    getRoleDetails(id) {
        this.service.getRoleDetails(id).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["first"])())
            .subscribe(response => {
            this.roleDetails = response;
            this.clearFormArray(this.permission());
            this.roleForm.patchValue(this.roleDetails);
            this.roleDetails.perm_list.forEach((element, k) => {
                this.addPermission();
                element.privilege.forEach((privilege, index) => {
                    this.addPrev(k, privilege);
                    if (privilege.subPrivilege && privilege.subPrivilege.length) {
                        privilege.subPrivilege.forEach((el) => {
                            this.addSubPrev(k, index, el);
                        });
                    }
                });
            });
        }, error => {
            this.toastr.errorToastr(error);
        });
    }
    formSubmit() {
        const requestPayload = { id: this.roleForm.value.id, title: this.roleForm.value.title, perm_list: this.roleForm.value.permissions };
        if (this.roleForm.valid) {
            this.service.saveRole(requestPayload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["first"])())
                .subscribe(response => {
                this.toastr.successToastr(!this.isUpdate ? 'Role added successfully' : 'Role updated successfully');
                this.router.navigateByUrl('/home/roles/roles-listing');
            }, error => {
                this.toastr.errorToastr(error.error.message);
            });
        }
        else {
            this.utility.validateAllFormFields(this.roleForm);
        }
    }
    nodeSelect(event) {
    }
    nodeUnselect(event) {
        this.roleForm.get('perm_list').setValue(this.selectedFiles2);
    }
};
AddRolesComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_4__["ToastrManager"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_5__["UtilityService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _services_app_service__WEBPACK_IMPORTED_MODULE_7__["AppService"] },
    { type: src_app_shared_nodeService__WEBPACK_IMPORTED_MODULE_6__["NodeService"] }
];
AddRolesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-roles',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-roles.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.html")).default,
        providers: [src_app_shared_nodeService__WEBPACK_IMPORTED_MODULE_6__["NodeService"]],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-roles.component.css */ "./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.css")).default]
    })
], AddRolesComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.css ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3JvbGVzL3BhZ2VzL3JvbGVzLWxpc3Qvcm9sZXMtbGlzdC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.ts ***!
  \**********************************************************************************/
/*! exports provided: RolesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesListComponent", function() { return RolesListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../services/app.service */ "./src/app/services/app.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng6-toastr-notifications */ "./node_modules/ng6-toastr-notifications/fesm2015/ng6-toastr-notifications.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/api.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(primeng_api__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_jwt_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/jwt.service */ "./src/app/services/jwt.service.ts");









let RolesListComponent = class RolesListComponent {
    constructor(service, toastr, router, jwtService, confirmationService) {
        this.service = service;
        this.toastr = toastr;
        this.router = router;
        this.jwtService = jwtService;
        this.confirmationService = confirmationService;
        this.isModalOpen = false;
        this.agents = [];
        this.first = 0;
        this.rows = 10;
        this.usersList = [];
        // tslint:disable-next-line:variable-name
        this._unsubscribe = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    ngOnInit() {
        this.usersList = [
            {
                name: 'role 1',
                no: '123',
                email: 'abc123@gmail.com'
            },
            {
                name: 'role 2',
                no: '123',
                email: 'abc123@gmail.com'
            }
        ];
        this.getAllRoles();
    }
    getAllRoles() {
        this.service.getAllRole().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
            .subscribe(response => {
            this.usersList = response;
        }, error => {
            this.toastr.errorToastr(error.error.message);
        });
    }
    getRoleDetails(path, userId, isDeletable) {
        if (!isDeletable) {
            return false;
        }
        this.router.navigate([path, userId]);
    }
    deleteRole(event, roleId) {
        event.preventDefault();
        event.stopPropagation();
        this.confirmationService.confirm({
            message: 'Are you sure that you want to delete this role?',
            accept: () => {
                this.service.deleteRole(roleId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["first"])())
                    .subscribe(response => {
                    this.toastr.successToastr('Role deleted successfully');
                    this.getAllRoles();
                }, error => {
                    this.toastr.errorToastr(error.error.message);
                });
                // Actual logic to perform a confirmation
            }
        });
    }
    next() {
        this.first = this.first + this.rows;
    }
    prev() {
        this.first = this.first - this.rows;
    }
    reset() {
        this.first = 0;
    }
    isLastPage() {
        return this.usersList ? this.first === (this.usersList.length - this.rows) : true;
    }
    isFirstPage() {
        return this.usersList ? this.first === 0 : true;
    }
    showAgents(event, index) {
        event.preventDefault();
        event.stopPropagation();
        this.agents = this.usersList[index].agent_list;
        if (this.agents.length) {
            this.isModalOpen = true;
        }
    }
    closeModal() {
        this.agents = [];
        this.isModalOpen = false;
    }
    ngOnDestroy() {
        this._unsubscribe.next(true);
        this._unsubscribe.complete();
    }
};
RolesListComponent.ctorParameters = () => [
    { type: _services_app_service__WEBPACK_IMPORTED_MODULE_3__["AppService"] },
    { type: ng6_toastr_notifications__WEBPACK_IMPORTED_MODULE_5__["ToastrManager"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: src_app_services_jwt_service__WEBPACK_IMPORTED_MODULE_8__["JwtService"] },
    { type: primeng_api__WEBPACK_IMPORTED_MODULE_6__["ConfirmationService"] }
];
RolesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-roles-list',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./roles-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./roles-list.component.css */ "./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.css")).default]
    })
], RolesListComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.css ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvbWUtbGF5b3V0L3BhZ2VzL3JvbGVzL3JvbGVzLWxheW91dC9yb2xlcy1sYXlvdXQuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.ts ***!
  \********************************************************************************/
/*! exports provided: RolesLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesLayoutComponent", function() { return RolesLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RolesLayoutComponent = class RolesLayoutComponent {
    constructor() { }
    ngOnInit() {
    }
};
RolesLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-roles-layout',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./roles-layout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./roles-layout.component.css */ "./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.css")).default]
    })
], RolesLayoutComponent);



/***/ }),

/***/ "./src/app/home-layout/pages/roles/roles-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/home-layout/pages/roles/roles-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: RolesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesRoutingModule", function() { return RolesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _roles_layout_roles_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./roles-layout/roles-layout.component */ "./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.ts");
/* harmony import */ var _pages_roles_list_roles_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages/roles-list/roles-list.component */ "./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.ts");
/* harmony import */ var _pages_add_roles_add_roles_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/add-roles/add-roles.component */ "./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.ts");






const routes = [
    {
        path: "",
        component: _roles_layout_roles_layout_component__WEBPACK_IMPORTED_MODULE_3__["RolesLayoutComponent"],
        children: [
            {
                path: "roles-listing",
                component: _pages_roles_list_roles_list_component__WEBPACK_IMPORTED_MODULE_4__["RolesListComponent"]
            },
            {
                path: "add-role",
                component: _pages_add_roles_add_roles_component__WEBPACK_IMPORTED_MODULE_5__["AddRolesComponent"]
            },
            {
                path: "edit-role/:id",
                component: _pages_add_roles_add_roles_component__WEBPACK_IMPORTED_MODULE_5__["AddRolesComponent"]
            },
            {
                path: "",
                pathMatch: "full",
                redirectTo: "roles-listing"
            }
        ]
    }
];
let RolesRoutingModule = class RolesRoutingModule {
};
RolesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], RolesRoutingModule);



/***/ }),

/***/ "./src/app/home-layout/pages/roles/roles.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/home-layout/pages/roles/roles.module.ts ***!
  \*********************************************************/
/*! exports provided: RolesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RolesModule", function() { return RolesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _roles_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./roles-routing.module */ "./src/app/home-layout/pages/roles/roles-routing.module.ts");
/* harmony import */ var _roles_layout_roles_layout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./roles-layout/roles-layout.component */ "./src/app/home-layout/pages/roles/roles-layout/roles-layout.component.ts");
/* harmony import */ var _pages_roles_list_roles_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pages/roles-list/roles-list.component */ "./src/app/home-layout/pages/roles/pages/roles-list/roles-list.component.ts");
/* harmony import */ var _pages_add_roles_add_roles_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/add-roles/add-roles.component */ "./src/app/home-layout/pages/roles/pages/add-roles/add-roles.component.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/tree */ "./node_modules/primeng/tree.js");
/* harmony import */ var primeng_tree__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_tree__WEBPACK_IMPORTED_MODULE_9__);










let RolesModule = class RolesModule {
};
RolesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_roles_layout_roles_layout_component__WEBPACK_IMPORTED_MODULE_4__["RolesLayoutComponent"], _pages_roles_list_roles_list_component__WEBPACK_IMPORTED_MODULE_5__["RolesListComponent"], _pages_add_roles_add_roles_component__WEBPACK_IMPORTED_MODULE_6__["AddRolesComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _roles_routing_module__WEBPACK_IMPORTED_MODULE_3__["RolesRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"],
            primeng_tree__WEBPACK_IMPORTED_MODULE_9__["TreeModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ]
    })
], RolesModule);



/***/ }),

/***/ "./src/app/shared/nodeService.ts":
/*!***************************************!*\
  !*** ./src/app/shared/nodeService.ts ***!
  \***************************************/
/*! exports provided: NodeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NodeService", function() { return NodeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let NodeService = class NodeService {
    constructor(http) {
        this.http = http;
    }
    getFiles() {
        return this.http.get('./assets/files.json')
            .toPromise()
            .then(res => res.data);
    }
};
NodeService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
NodeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], NodeService);



/***/ })

}]);
//# sourceMappingURL=pages-roles-roles-module-es2015.js.map