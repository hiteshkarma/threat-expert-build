var newThreat = ''; 
function drawPie(obj) {
    var container = obj.container
    container.innerHTML = ''
    var chartHolder = createElement(
        'div',
        {class: 'pie-chart-holder'},
        container
    )
    var data = obj.data
    var width = obj.width
    var height = obj.height
    var radius = obj.radius
    var canvas = Snap(width, height)
    chartHolder.appendChild(canvas.node)
    var legendsElement = createElement('div', {class: 'pie-legends'}, container)
    var cx = width / 2
    var cy = height / 2
    var total = 0
    for (var i = 0; i < data.length; i++) {
        var value = data[i].value
        if (value == undefined) {
            value = 1
        }
        total += value
    }
    var ang = 0
    var colors = ['#52D726', '#FF7300', '#FF0000', '#007ED6', '#7CDDDD', '#dad306cf', '#3747ffbd', '#a5a5a5eb', '#5c5d57d9', '#98e740c4', '#d833d2c4',
    '#c81dc2c4', '#ba0eb4c4', '#a907a3c4', '#7a0375c4', '#650161c4', '#8e6a8dc4', '#024c55c4', '#0b8999c4', '#0fb7ccc4', '#07dbf5c4', '#3f7278c4', '#658386c4',
    '#b31717c4', '#950d0dc4', '#740909c4', '#672424c4', '#444444', '#777777', '#999999', '#aaa']
    for (var i = 0; i < data.length; i++) {
        var d = data[i]
        var value = d.value
        if (value == undefined) {
            value = 1
        }
        var color = d.color
        if (color == undefined) {
            color = colors[i % data.length]
        }
        var sectorAng = (360 * value) / total
        var path = canvas.path()
        if (sectorAng == 360) {
            path = canvas.circle(cx, cy, radius)
            path.attr({
                class: 'pie-sector',
                fill: color
            })
        } else {
            path = canvas.path()
            path.attr({
                d: describeArc(cx, cy, radius, ang, ang + sectorAng),
                class: 'pie-sector',
                fill: color
            })
        }

        ang = ang + sectorAng
        if (obj.callback) {
            path.node.addEventListener(
                'click',
                (function(d) {
                    return function() {
                        obj.callback(d)
                    }
                })(d)
            )
        }
        var legLabel = d.label
        if (d.value) {
            legLabel += ' (' + value + ')'
        }
        var legendElement_ = createElement(
            'span',
            {class: 'pie-legend'},
            legendsElement
        )
        var spanElem = createElement(
            'a',
            {class: 'pie-legend-symbol'},
            legendElement_
        );
        spanElem.style.backgroundColor = color;
        spanElem.style.padding = '2px'
        spanElem.style.display = 'inline-block';
        createElement(
            'span',
            {class: 'pie-legend-text'},
            legendElement_,
            legLabel
        )
    }
}
function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0

    return {
        x: centerX + radius * Math.cos(angleInRadians),
        y: centerY + radius * Math.sin(angleInRadians)
    }
}

function describeArc(x, y, radius, startAngle, endAngle) {
    var start = polarToCartesian(x, y, radius, endAngle)
    var end = polarToCartesian(x, y, radius, startAngle)

    var arcSweep = endAngle - startAngle <= 180 ? '0' : '1'

    var d = [
        'M',
        start.x,
        start.y,
        'A',
        radius,
        radius,
        0,
        arcSweep,
        0,
        end.x,
        end.y,
        'L',
        x,
        y,
        'L',
        start.x,
        start.y
    ].join(' ')

    return d
}
function createElement(type, att, parent, inn) {
    var e = document.createElement(type)

    if (inn !== undefined) {
        e.innerHTML = inn
    }
    for (var i in att) {
        e.setAttribute(i, att[i])
    }
    parent.appendChild(e)
    return e
}
