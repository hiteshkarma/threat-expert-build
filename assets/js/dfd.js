var dfdData = '';

function DFD(ele, elementsJSON) {
    var self = this
    clearElement(ele)
    var canvasEle = createElement('div', {class: 'dfd-canvas'}, ele)
    self.width = canvasEle.clientWidth
    self.height = canvasEle.clientHeight
    self.cx = self.width / 2
    self.cy = self.height / 2
    var canvas = Snap(self.width, self.height)
    canvasEle.appendChild(canvas.node)
    self.canvas = canvas
    self.panCanvas = canvas.g()
    self.zoomCanvas = self.panCanvas.g()
    self.previewLine = self.zoomCanvas.path()
    self.linksCanvas = self.zoomCanvas.g()
    self.boundaryCanvas = self.zoomCanvas.g()

    self.elementsPaper = self.zoomCanvas.g()
    self.linksTextPaper = self.zoomCanvas.g()
    var markerPath = canvas
        .path('M 100 59 L 293 148 L 100 243 L 100 151 Z')
        .attr({class: 'mid-marker'})
    var markerPath_ = canvas
        .path('M 100 59 L 293 148 L 100 243 L 100 151 Z')
        .attr({class: 'mid-marker'})
    self.endMarker = markerPath.marker(0, 0, 4000, 4000, 250, 150).attr({
        markerUnits: 'strokeWidth',
        markerWidth: 200,
        markerHeight: 200,
        orient: 'auto'
    })
    self.startMarker = markerPath_.marker(0, 0, 4000, 4000, 150, 150).attr({
        markerUnits: 'strokeWidth',
        markerWidth: 200,
        markerHeight: 200,
        orient: 'auto'
    })
    self.previewLine.attr({
        class: 'dfd-preview-line'
        //markerEnd: self.midMarker
    })
    var elementsContainer = createElement('div', {class: 'dfd-elements'}, ele)
    var elementsSearchContainer = createElement(
        'div',
        {class: 'dfd-elements-search-container'},
        elementsContainer
    )
    var elementsSearchInput = createElement(
        'input',
        {class: 'dfd-elements-search-input', placeholder: 'Search'},
        elementsSearchContainer
    )
    var groupContainers = {}
    function checkInput() {
        var inputValue = String(elementsSearchInput.value).trim()
        for (var x = 0; x < renderedList.length; x++) {
            renderedList[x].element.classList.add('dfd-element-hide')
            if (
                inputValue == '' ||
                String(renderedList[x].name)
                    .toLowerCase()
                    .indexOf(String(inputValue).toLowerCase()) >= 0
            ) {
                renderedList[x].element.classList.remove('dfd-element-hide')
            }
        }
        forList(groupContainers, function(g) {
            g.container.style.removeProperty('display')
            if (
                g.containerList.querySelectorAll('.dfd-element-hide').length ==
                g.containerList.querySelectorAll('.dfd-element').length
            ) {
                g.container.style.display = 'none'
            }
        })
    }
    createElement(
        'button',
        {class: 'dfd-elements-search-clear'},
        elementsSearchContainer,
        '&#10006;'
    ).addEventListener('click', function() {
        elementsSearchInput.value = ''
        checkInput()
    })
    var renderedList = []
    elementsSearchInput.addEventListener('keyup', checkInput)
    var elementsList = createElement(
        'div',
        {class: 'dfd-elements-list'},
        elementsContainer
    )
    var elementHover = createElement(
        'div',
        {class: 'dfd-element-hover'},
        elementsContainer
    )
    elementsList.addEventListener('mousemove', function() {
        elementHover.classList.remove('dfd-element-hover-visible')
        clearElement(elementHover)
    })
    var elements = [
        // {name: 'rectangle', shape: 'rectangle'},
        // {name: 'circle', shape: 'circle'},
        // {name: 'circle-double', shape: 'circle-double'},
    ]
    // toDataUrl(elements[0].imageURL, function(dataURL) {
    //     elements[0].imageURL = dateURL
    // })
    self.dataflowComp = {}
    forList(elementsJSON, function(comp) {
        var name = Object.keys(comp)[0]
        var obj = {
            name: name,
            imageURL: comp.icon,
            form: JSON.parse(JSON.stringify(comp[name])),
            messageText: name,
            inScope: true,
            group: comp.group
        }

        if (name == 'Dataflow') {
            self.dataflowComp = comp[name]
        } else {
            elements.push(obj)
        }
    })
    elements.push({
        name: 'rectangle-dotted',
        shape: 'rectangle-dotted',
        empty: true,
        messageText: 'Boundary',
        class: 'dfd-rectangle-dotted',
        imageURL: 'assets/images/rectangle.svg',
        usePath: true,
        group: 'no group'
    })

    function renderElements() {
        for (var i = 0; i < elements.length; i++) {
            var group = elements[i].group
            if (groupContainers[group] == undefined) {
                groupContainers[group] = {}
                groupContainers[group].container = createElement(
                    'div',
                    {class: 'dfd-element-group'},
                    elementsList
                )

                var groupNameContainer = createElement(
                    'div',
                    {class: 'dfd-element-group-name'},
                    groupContainers[group].container,
                    group
                )
                var groupBtn = createElement(
                    'button',
                    {class: 'dfd-element-group-button'},
                    groupNameContainer,
                    '&#x25B2;'
                )
                groupContainers[group].expand = true
                groupBtn.addEventListener(
                    'click',
                    (function(group) {
                        return function() {
                            if (groupContainers[group].expand) {
                                groupContainers[group].expand = false
                                this.innerHTML = '&#x25BC;'
                                groupContainers[
                                    group
                                ].containerList.classList.add(
                                    'dfd-element-group-list-hidden'
                                )
                            } else {
                                groupContainers[group].expand = true
                                this.innerHTML = '&#x25B2;'
                                groupContainers[
                                    group
                                ].containerList.classList.remove(
                                    'dfd-element-group-list-hidden'
                                )
                            }
                        }
                    })(group)
                )
                groupContainers[group].containerList = createElement(
                    'div',
                    {class: 'dfd-element-group-list'},
                    groupContainers[group].container
                )
            }
            var element = createElement(
                'div',
                {class: 'dfd-element'},
                groupContainers[group].containerList
            )

            var paper = Snap(30, 30)
            element.appendChild(paper.node)
            self.addNode(elements[i], {
                width: 20,
                height: 20,
                paper: paper
            })

            element.addEventListener(
                'click',
                (function(d) {
                    return function() {
                        self.addNode(d)
                    }
                })(elements[i])
            )
            element.addEventListener(
                'mousemove',
                (function(d) {
                    return function(event) {
                        event.stopPropagation()
                        clearElement(elementHover)
                        // elementHover.style.top =
                        //     elementOffsets(
                        //         groupContainers[d.group].container
                        //     )[1] + 'px'
                        groupContainers[d.group].container.appendChild(
                            elementHover
                        )
                        elementHover.classList.add('dfd-element-hover-visible')
                        var paper = Snap(100, 100)
                        elementHover.appendChild(paper.node)
                        self.addNode(d, {
                            width: 90,
                            height: 90,
                            paper: paper
                        })
                        createElement(
                            'div',
                            {class: 'dfd-element-name'},
                            elementHover,
                            d.name
                        )
                    }
                })(elements[i])
            )
            renderedList.push({element: element, name: elements[i].name})
        }
    }
    //elements = elements
    self.selectInfoEle = createElement('div', {class: 'dfd-select-info'}, ele)
    self.initSelectInfoEle()
    var loadedElements = 0
    forList(elements, function(element) {
        if (element.usePath) {
            loadShape(element.imageURL, function(p) {
                element.pathInfo = p
                loadedElements++
                if (loadedElements == elements.length) {
                    renderElements()
                }
            })
        } else {
            toDataUrl(element.imageURL, function(dataURL) {
                element.imageURL = dataURL
                loadedElements++
                if (loadedElements == elements.length) {
                    renderElements()
                }
            })
        }
    })

    this.nodes = {}
    this.links = []

    window.addEventListener('mousemove', function(event) {
        if (self.moveObj.mode !== 'move') {
            return
        }
        if (self.nodes[self.moveObj.object.id] == undefined) {
            return
        }
        var diffX = self.moveObj.clientX - event.clientX
        var diffY = self.moveObj.clientY - event.clientY
        var o = self.moveObj.object
        var g = o.g
        var dim = o.dim
        var dim_ = self.moveObj.object_.dim
        dim.x = dim_.x - diffX
        dim.y = dim_.y - diffY
        g.attr({
            transform: 'translate(' + dim.x + ' ' + dim.y + ')'
        })

        self.updateLinks()
    })
    window.addEventListener('mousemove', function(event) {
        if (self.moveObj.mode !== 'link') {
            return
        }
        var o = self.moveObj.object
        var dim = o.dim
        var ofs = elementOffsets(self.canvas.node.parentNode)
        var x2 = event.clientX - ofs[0]
        var y2 = event.clientY - ofs[1]
        var x1 = dim.x + dim.width / 2
        var y1 = dim.y + dim.height / 2

        if (self.moveObj.linkType == 'top') {
            y1 = dim.y
        }
        if (self.moveObj.linkType == 'left') {
            x1 = dim.x
        }
        if (self.moveObj.linkType == 'right') {
            x1 = dim.x + dim.width
        }
        if (self.moveObj.linkType == 'bottom') {
            y1 = dim.y + dim.height
        }
        var dx = x2 - self.gx * (self.scale / 2) - x1
        var dy = y2 + self.gy - y1
        // dx /= self.scale * 2
        // dy /= self.scale * 2
        var x2_ = x1 + dx
        var y2_ = dy + y1
        if (self.scale == 1) {
            self.previewLine.attr({
                d: self.linkPath(x1, y1, x2_, y2_)
            })
        }
    })
    window.addEventListener('mousemove', function(event) {
        if (
            [
                'resize-nw',
                'resize-ne',
                'resize-sw',
                'resize-se',
                'resize-s',
                'resize-n',
                'resize-w',
                'resize-e'
            ].indexOf(self.moveObj.mode) < 0
        ) {
            return
        }
        var diffX = self.moveObj.clientX - event.clientX
        var diffY = self.moveObj.clientY - event.clientY
        var o = self.moveObj.object
        var dim_ = self.moveObj.object_.dim
        var g = o.g
        var dim = o.dim
        var ratio = dim_.width / dim_.height
        if (['resize-nw'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio = (dim_.width + diffX) / (dim_.height + diffY)
                if (ratio < newRatio) {
                    diffX = ratio * (dim_.height + diffY) - dim_.width
                } else {
                    diffY = (dim_.width + diffX) / ratio - dim_.height
                }
            }
            dim.x = dim_.x - diffX
            dim.y = dim_.y - diffY
            dim.width = dim_.width + diffX
            dim.height = dim_.height + diffY
        }
        if (['resize-ne'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio = (dim_.width - diffX) / (dim_.height + diffY)
                if (ratio < newRatio) {
                    diffX = -1 * (ratio * (dim_.height + diffY) - dim_.width)
                } else {
                    diffY = (dim_.width - diffX) / ratio - dim_.height
                }
            }
            dim.y = dim_.y - diffY
            dim.width = dim_.width - diffX
            dim.height = dim_.height + diffY
        }
        if (['resize-sw'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio = (dim_.width + diffX) / (dim_.height - diffY)
                if (ratio < newRatio) {
                    diffX = ratio * (dim_.height - diffY) - dim_.width
                } else {
                    diffY = -1 * ((dim_.width + diffX) / ratio - dim_.height)
                }
            }
            dim.x = dim_.x - diffX
            dim.width = dim_.width + diffX
            dim.height = dim_.height - diffY
        }
        if (['resize-se'].indexOf(self.moveObj.mode) >= 0) {
            if (event.ctrlKey) {
                var newRatio = (dim_.width - diffX) / (dim_.height - diffY)
                if (ratio < newRatio) {
                    diffX = -1 * (ratio * (dim_.height - diffY) - dim_.width)
                } else {
                    diffY = -1 * ((dim_.width - diffX) / ratio - dim_.height)
                }
            }
            dim.width = dim_.width - diffX
            dim.height = dim_.height - diffY
        }
        if (['resize-n'].indexOf(self.moveObj.mode) >= 0) {
            dim.y = dim_.y - diffY
            dim.height = dim_.height + diffY
        }
        if (['resize-w'].indexOf(self.moveObj.mode) >= 0) {
            dim.x = dim_.x - diffX
            dim.width = dim_.width + diffX
        }
        if (['resize-e'].indexOf(self.moveObj.mode) >= 0) {
            dim.width = dim_.width - diffX
        }
        if (['resize-s'].indexOf(self.moveObj.mode) >= 0) {
            dim.height = dim_.height - diffY
        }
        g.attr({
            transform: 'translate(' + dim.x + ' ' + dim.y + ')'
        })
        self.updateResizeObj(o)
        o.draw(o)
        self.updateLinks()
    })

    window.addEventListener('mouseup', function() {
        self.moveObj = {}
        self.previewLine.attr({d: ''})
    })
    self.moveObj = {}
    window.addEventListener('click', function(event) {
        self.clearSelect()
    })
    window.addEventListener('mousemove', function(event) {
        if (self.moveObj.mode != 'link') {
            self.clearHover()
        }
    })
    self.gx = 0
    self.gy = 0
    canvas.node.addEventListener('mousedown', function(event) {
        self.moveObj = {
            object: self.panCanvas,
            event: event,
            cx: self.gx,
            cy: self.gy,
            clientX: event.clientX,
            clientY: event.clientY,
            mode: 'pan'
        }
    })
    window.addEventListener('mousemove', function(event) {
        if (self.moveObj.mode !== 'pan') {
            return
        }
        var diffX = event.clientX - self.moveObj.clientX
        var diffY = event.clientY - self.moveObj.clientY
        self.gx = self.moveObj.cx + diffX
        self.gy = self.moveObj.cy + diffY
        self.panCanvas.attr({
            transform: 'translate(' + self.gx + ',' + self.gy + ')'
        })
    })
    self.scale = 1
    canvas.node.addEventListener('wheel', function(event) {
        return
        var scaleDiff = -(0.25 * event.deltaY) / 100
        if (self.scale + scaleDiff < 1) {
            scaleDiff = 1 - self.scale
        }
        self.scale += scaleDiff

        self.gx -= (scaleDiff * self.width) / 2
        self.gy -= (scaleDiff * self.height) / 2
        self.panCanvas.attr(
            {
                transform: 'translate(' + self.gx + ',' + self.gy + ')'
            },
            10
        )
        self.zoomCanvas.attr(
            {
                transform: 'scale(' + self.scale + ')'
            },
            10
        )
        //console.log(event.deltaY)
    })
}
DFD.prototype.initSelectInfoEle = function() {
    var self = this
    var parent = self.selectInfoEle
    parent.addEventListener('click', function(event) {
        event.stopPropagation()
    })
    var nameEle = createElement(
        'div',
        {class: 'dfd-info-name'},
        parent,
        '<label>Name</label>'
    )
    createElement(
        'input',
        {class: 'dfd-shape-message-editor'},
        nameEle
    ).addEventListener('keyup', function() {
        self.selectedObject.messageText = this.value

        if (self.selectedObject.linkCount != undefined) {
            self.updateText(
                '(' +
                    self.selectedObject.linkCount +
                    ') ' +
                    self.selectedObject.messageText,
                self.selectedObject.message
            )
        } else {
            self.updateText(
                self.selectedObject.messageText,
                self.selectedObject.message
            )
        }
        self.messageBox(self.selectedObject)
        // if (self.selectedObject.messageRect) {
        //     var bBox = self.selectedObject.message.getBBox()
        //     self.selectedObject.messageRect.attr({
        //         x: self.selectedObject.dim.width / 2 - bBox.width / 2,
        //         y: self.selectedObject.dim.height,
        //         width: bBox.width,
        //         height: bBox.height
        //     })
        //     insertBefore(
        //         self.selectedObject.message.node,
        //         self.selectedObject.messageRect.node
        //     )
        // }
    })
    var indexEle = createElement(
        'div',
        {class: 'dfd-info-name'},
        parent,
        '<label>Index</label>'
    )
    createElement(
        'input',
        {class: 'dfd-shape-index-editor'},
        indexEle
    ).addEventListener('keyup', function() {
        self.selectedObject.linkCount = this.value

        self.updateText(
            '(' +
                self.selectedObject.linkCount +
                ') ' +
                self.selectedObject.messageText,
            self.selectedObject.message
        )
    })

    var shapeDiv = createElement(
        'div',
        {class: 'dfd-info-shape-containers-inner'},
        createElement('div', {class: 'dfd-info-shape-containers'}, parent)
    )

    createElement('div', {class: 'dfd-info-type'}, shapeDiv, 'Type')

    var scopeContainer = createElement(
        'div',
        {class: 'dfd-shape-scope'},
        shapeDiv
    )

    createElement('span', {}, scopeContainer, 'in scope')
    createElement('input', {type: 'checkbox'}, scopeContainer).addEventListener(
        'click',
        function() {
            var value = this.checked
            var element = self.selectedObject.element
            element.inScope = value
        }
    )
    createElement(
        'div',
        {class: 'dfd-shape-form'},
        createElement('div', {class: 'dfd-shape-form-outer'}, shapeDiv)
    )

    var iconsRow = createElement('div', {class: 'dfd-info-icons'}, shapeDiv)
    function getNode() {
        var node = {}
        if (self.selectedObject.g) {
            node = self.selectedObject.g.node
        } else {
            node = self.selectedObject.message.node
        }
        return node
    }
    createElement(
        'button',
        {class: 'dfd-info-icon'},
        iconsRow,
        'Bring Front'
    ).addEventListener('click', function(event) {
        var node = getNode()
        insertAfter(node.parentNode.lastChild, node)
        event.stopPropagation()
    })
    createElement(
        'button',
        {class: 'dfd-info-icon'},
        iconsRow,
        'Bring Forward'
    ).addEventListener('click', function(event) {
        var node = getNode()
        event.stopPropagation()
        insertAfter(node.nextSibling, node)
    })
    createElement(
        'button',
        {class: 'dfd-info-icon'},
        iconsRow,
        'Send Backward'
    ).addEventListener('click', function(event) {
        var node = getNode()
        event.stopPropagation()
        insertBefore(node.previousSibling, node)
    })
    createElement(
        'button',
        {class: 'dfd-info-icon'},
        iconsRow,
        'Send Back'
    ).addEventListener('click', function(event) {
        var node = getNode()
        event.stopPropagation()
        insertBefore(node.parentNode.firstChild, node)
    })

    createElement(
        'button',
        {class: 'dfd-remove-icon'},
        parent,
        'Remove'
    ).addEventListener('click', function(event) {
        event.stopPropagation()
        if (self.selectedObject.id) {
            var links = []
            for (var i = 0; i < self.links.length; i++) {
                var link = self.links[i]
                if (
                    link.source.id != self.selectedObject.id &&
                    link.target.id != self.selectedObject.id
                ) {
                    links.push(link)
                } else {
                    var path = link.path.node
                    var text = link.message.node
                    path.parentNode.removeChild(path)
                    text.parentNode.removeChild(text)
                }
            }
            self.links = links
            var node = self.selectedObject.g.node
            node.parentNode.removeChild(node)
            delete self.nodes[self.selectedObject.id]
        }
        if (self.selectedObject.source) {
            var links = []
            for (var i = 0; i < self.links.length; i++) {
                var link = self.links[i]
                if (
                    link.source.id == self.selectedObject.source.id &&
                    link.target.id == self.selectedObject.target.id
                ) {
                    var path = link.path.node
                    var text = link.message.node
                    path.parentNode.removeChild(path)
                    text.parentNode.removeChild(text)
                } else {
                    links.push(link)
                }
            }
            self.links = links
        }
        self.selectInfoEle.classList.remove('dfd-select-info-visible')
    })
}
DFD.prototype.updateText = function(val, message) {
    // var arr = val.split('\n')
    // clearElement(message.node)
    // for (var i = 0; i < arr.length; i++) {
    //     createElement(
    //         'tspan',
    //         {y: parseFloat(message.attr('y')) + i * 15, x: message.attr('x')},
    //         message.node,
    //         arr[i]
    //     )
    // }
    message.node.innerHTML = val
}
DFD.prototype.onHover = function(obj) {
    var self = this

    if (obj.g.node.classList.contains('dfd-shape-selected')) {
        self.clearHover()
        return
    }
    self.hoverObject = obj
    self.hoverObject.g.node.classList.add('dfd-hover-info-visible')
}
DFD.prototype.onSelect = function(obj) {
    var self = this
    self.selectedObject = obj

    self.selectInfoEle.classList.add('dfd-select-info-visible')
    // self.selectInfoEle
    //     .querySelector('.dfd-info-shape-containers')
    //     .style.removeProperty('display')
    // self.selectInfoEle.querySelector(
    //     '.dfd-info-shape-containers'
    // ).style.display = 'none'
    if (obj.element) {
        if (obj.g) {
            obj.g.node.classList.add('dfd-shape-selected')
        }
        self.selectInfoEle.querySelector('.dfd-info-type').innerHTML =
            'Type (' + obj.element.name + ')'

        self.selectInfoEle.querySelector('.dfd-shape-scope input').checked =
            obj.element.inScope

        // self.selectInfoEle
        //     .querySelector('.dfd-info-shape-containers')
        //     .style.removeProperty('display')
        //dfd-shape-form
        var formEle = self.selectInfoEle.querySelector('.dfd-shape-form')
        clearElement(formEle)
        forList(obj.element.form, function(defaultValue, key) {
            if (key == 'inScope') {
                return
            }
            var formRow = createElement('div', {class: 'dfd-form-row'}, formEle)
            createElement('div', {class: 'dfd-form-row-name'}, formRow, key)
            var selectEle = createElement(
                'select',
                {class: 'dfd-form-row-sel'},
                formRow
            )
            createElement('option', {value: null}, selectEle, 'NA')
            createElement('option', {value: true}, selectEle, 'True')
            createElement('option', {value: false}, selectEle, 'False')
            selectEle.value = defaultValue
            selectEle.addEventListener('change', function() {
                var value = selectEle.value
                if (value == 'null') {
                    value = null
                } else if (value == 'true') {
                    value = true
                } else {
                    value = false
                }
                obj.element.form[key] = value
            })
        })
        self.selectInfoEle.querySelector(
            '.dfd-shape-index-editor'
        ).parentNode.style.display = 'none'
    }
    if (self.selectedObject.linkCount != undefined) {
        self.selectInfoEle.querySelector('.dfd-shape-index-editor').value =
            self.selectedObject.linkCount
        self.selectInfoEle
            .querySelector('.dfd-shape-index-editor')
            .parentNode.style.removeProperty('display')
    }
    self.selectInfoEle.querySelector('.dfd-shape-message-editor').value =
        obj.messageText
    self.clearHover()
}
DFD.prototype.clearSelect = function() {
    var self = this
    self.selectInfoEle.classList.remove('dfd-select-info-visible')
    if (self.selectedObject == undefined) {
        return
    }
    var obj = self.selectedObject
    if (obj.g && obj.g.node) {
        obj.g.node.classList.remove('dfd-shape-selected')
    }
    delete self.selectedObject
}
DFD.prototype.clearHover = function() {
    var self = this
    if (self.hoverObject == undefined) {
        return
    }
    self.hoverObject.g.node.classList.remove('dfd-hover-info-visible')

    delete self.hoverObject
}
DFD.prototype.linkPath = function(x1, y1, x2, y2, obj, retMId) {
    var self = this

    var x1_ = x1
    var x2_ = x2
    var y1_ = y1
    var y2_ = y2
    var textAnchor = 'start'
    if (obj) {
        var offset = 10
        var sourceType = obj.sourceType
        var targetType = obj.targetType
        if (sourceType == 'right') {
            x1 = x1_ + offset
        }
        if (sourceType == 'left') {
            x1 = x1_ - offset
        }
        if (sourceType == 'top') {
            y1 = y1_ - offset
        }
        if (sourceType == 'bottom') {
            y1_ += 30
            y1 = y1_ + offset
        }
        if (targetType == 'right') {
            x2 = x2_ + offset
        }
        if (targetType == 'left') {
            x2 = x2_ - offset
            textAnchor = 'end'
        }
        if (targetType == 'top') {
            y2 = y2_ - offset
        }
        if (targetType == 'bottom') {
            y2_ += 30
            y2 = y2_ + offset
        }
    }
    var midX = (x1 + x2) / 2
    var midY = (y1 + y2) / 2
    if (retMId) {
        return {x: x2 + 3, y: y1 - 7, textAnchor: textAnchor}
    }
    var d =
        'M' +
        x1_ +
        ' ' +
        y1_ +
        ' L' +
        x1 +
        ' ' +
        y1 +
        ' L' +
        midX +
        ' ' +
        y1 +
        ' L' +
        x2 +
        ' ' +
        y1 +
        ' L' +
        x2 +
        ' ' +
        midY +
        ' L' +
        x2 +
        ' ' +
        y2 +
        ' L' +
        x2_ +
        ' ' +
        y2_
    return d
    // var xDIff = (x2 - x1) * 0.1
    // var yDIff = (y2 - y1) * 0.1
    // return self.drawPath([
    //     [x1, y1],
    //     [midX, y1 + yDIff],
    //     [x2 - xDIff, midY],
    //     [x2, y2]
    // ])
}
DFD.prototype.linkCorodinates = function(linkObj) {
    var source = linkObj.source
    var target = linkObj.target
    var x1 = source.dim.x + source.dim.width / 2
    var x2 = target.dim.x + target.dim.width / 2
    var y1 = source.dim.y + source.dim.height / 2
    var y2 = target.dim.y + target.dim.height / 2
    if (linkObj.sourceType == 'right') {
        x1 = source.dim.x + source.dim.width
    }
    if (linkObj.sourceType == 'left') {
        x1 = source.dim.x
    }
    if (linkObj.sourceType == 'top') {
        y1 = source.dim.y
    }
    if (linkObj.sourceType == 'bottom') {
        y1 = source.dim.y + source.dim.height
    }
    if (linkObj.targetType == 'right') {
        x2 = target.dim.x + target.dim.width
    }
    if (linkObj.targetType == 'left') {
        x2 = target.dim.x
    }
    if (linkObj.targetType == 'top') {
        y2 = target.dim.y
    }
    if (linkObj.targetType == 'bottom') {
        y2 = target.dim.y + target.dim.height
    }
    return {x1: x1, x2: x2, y1: y1, y2: y2}
}
DFD.prototype.createLink = function(source, target, object) {
    var self = this
    if (self.linkCount == undefined) {
        self.linkCount = 0
    }
    if (object.linkCount == undefined) {
        self.linkCount++
    }
    if (source.id === target.id) {
        return
    }
    if (source.element.empty) {
        return
    }
    if (target.element.empty) {
        return
    }

    for (var i = 0; i < self.links.length; i++) {
        var l = self.links[i]
        if (l.source.id == source.id && l.target.id == target.id) {
            return
        }
    }
    var linkObj = {}
    linkObj.source = source
    linkObj.target = target
    linkObj.sourceType = object.sourceType
    linkObj.targetType = object.targetType
    linkObj.element = {name: 'Dataflow', form: self.dataflowComp, inScope: true}
    if (object.element) {
        linkObj.element = object.element
    }
    var co = self.linkCorodinates(linkObj)
    var x1 = co.x1
    var x2 = co.x2
    var y1 = co.y1
    var y2 = co.y2
    linkObj.path = self.linksCanvas.path()
    linkObj.path.attr({
        class: 'dfd-preview-line',
        d: self.linkPath(x1, y1, x2, y2, linkObj),
        markerEnd: self.endMarker
        //markerStart: self.startMarker
    })
    linkObj.messageText = 'Message'
    linkObj.linkCount = self.linkCount

    if (object.linkCount != undefined) {
        linkObj.linkCount = object.linkCount
    }

    if (object != undefined) {
        if (object.messageText) {
            linkObj.messageText = object.messageText
        }
    }
    if (object.linkCount == undefined) {
        linkObj.messageText += ' ' + linkObj.linkCount
    }
    var messageCordinates = self.linkPath(x1, y1, x2, y2, linkObj, true)
    linkObj.message = self.tSpan(
        self.linksTextPaper,
        messageCordinates.x,
        messageCordinates.y,
        '(' + linkObj.linkCount + ') ' + linkObj.messageText
    )

    linkObj.message.attr({
        class: 'dfd-link-message',
        'text-anchor': messageCordinates.textAnchor
    })
    self.links.push(linkObj)
    linkObj.message.node.addEventListener('click', function(event) {
        event.stopPropagation()
        self.clearSelect()
        self.onSelect(linkObj)
    })
    return linkObj
}
DFD.prototype.updateLinks = function() {
    var self = this
    for (var i = 0; i < self.links.length; i++) {
        var l = self.links[i]
        var source = l.source
        var target = l.target
        // var x1 = source.dim.x + source.dim.width / 2
        // var x2 = target.dim.x + target.dim.width / 2
        // var y1 = source.dim.y + source.dim.height / 2
        // var y2 = target.dim.y + target.dim.height / 2
        var co = self.linkCorodinates(l)
        var x1 = co.x1
        var x2 = co.x2
        var y1 = co.y1
        var y2 = co.y2
        l.path.attr({
            d: self.linkPath(x1, y1, x2, y2, l)
        })
        var midX = (x1 + x2) / 2
        var midY = (y1 + y2) / 2
        var messageCordinates = self.linkPath(x1, y1, x2, y2, l, true)
        l.message.attr({
            x: messageCordinates.x,
            y: messageCordinates.y,
            'text-anchor': messageCordinates.textAnchor
        })

        self.updateText('(' + l.linkCount + ') ' + l.messageText, l.message)
    }
}

DFD.prototype.addNode = function(element, paperObj, object) {
    element = JSON.parse(JSON.stringify(element))
    var self = this
    var width = 50
    var height = 50
    if (paperObj) {
        width = paperObj.width
        height = paperObj.height
    }
    var dim = {
        x: self.cx - width / 2,
        y: self.cy - height / 2,
        width: width,
        height: height
    }
    var id = randomID()
    var g = undefined
    if (paperObj) {
        dim.x = 5
        dim.y = 5
        g = paperObj.paper.g()
    } else {
        if (element.empty) {
            g = self.boundaryCanvas.g()
        } else {
            g = self.elementsPaper.g()
        }
    }
    if (object != undefined) {
        if (object.dim) {
            dim = object.dim
        }
        if (object.id) {
            id = object.id
        }
    }
    g.attr({transform: 'translate(' + dim.x + ' ' + dim.y + ')'})

    var obj = {}
    obj.id = id
    obj.g = g
    obj.dim = dim
    obj.element = element
    obj.draw = function(o) {
        if (o.shapeGroup == undefined) {
            o.shapeGroup = o.g.g()
            if (element.pathInfo) {
                forList(element.pathInfo.pathAttributes, function(attributes) {
                    var pathObj = o.shapeGroup.path()
                    var obj = {'vector-effect': 'non-scaling-stroke'}
                    forList(attributes, function(value) {
                        obj[value[0]] = value[1]
                    })
                    pathObj.attr(obj)
                })
            } else {
                var bkRect = o.shapeGroup.rect(0, 0, 32, 32)
                bkRect.attr({class: 'dfd-shape-bk'})
                o.shapeGroup.image(element.imageURL, 0, 0, 32, 32)
            }
        }
        var xScale = o.dim.width / 32
        var yScale = o.dim.height / 32
        o.shapeGroup.attr({
            transform: 'scale(' + xScale + ',' + yScale + ')'
        })
    }

    if (obj.draw) {
        obj.draw(obj)
        if (element.pathInfo) {
            var shapeNodes = []
            var cls = 'dfd-shape'
            if (obj.element.class) {
                cls = obj.element.class
            }
            if (obj.shapeGroup) {
                shapeNodes = obj.shapeGroup.selectAll('path')

                for (var i = 0; i < shapeNodes.length; i++) {
                    shapeNodes[i].attr({class: cls})
                }
            }
        }
    }
    if (paperObj) {
        return
    }
    obj.messageText = 'Message'
    if (self.shapeIndex == undefined) {
        self.shapeIndex = 0
    }

    if (element.messageText != undefined) {
        obj.messageText = element.messageText
        var count = 0
        self.shapeIndex++
        forList(self.nodes, function(n) {
            if (n.element.name == element.name) {
                count++
            }
        })
        //if (count > 0) {
        obj.messageText = obj.messageText + ' ' + (count + 1)
        //}
    }
    if (object != undefined) {
        if (object.messageText) {
            obj.messageText = object.messageText
        }
    }

    obj.message = obj.g.text(
        obj.dim.width / 2,
        obj.dim.height + 20,
        obj.messageText
    )
    // if (obj.element.empty) {
    //     obj.message.attr({x: obj.dim.width / 2, y: -5})
    // }
    obj.message.attr({
        'text-anchor': 'middle',
        class: 'dfd-shape-message'
    })
    // var bBox = obj.message.getBBox()
    obj.messageRect = obj.g.rect()
    self.messageBox(obj)
    insertBefore(obj.message.node, obj.messageRect.node)
    obj.messageRect.attr({class: 'dfd-shape-bk'})
    function addEvents(node) {
        node.addEventListener('click', function(event) {
            self.clearSelect()
            self.onSelect(obj)
            event.stopPropagation()
        })
        node.addEventListener('mousemove', function(event) {
            self.clearHover()
            self.onHover(obj)
            if (self.moveObj.mode == undefined || self.moveObj.mode == 'pan') {
                event.stopPropagation()
            }
        })
        node.addEventListener('mousedown', function(event) {
            //if (!event.ctrlKey) {
            self.moveObj = {
                object: obj,
                object_: JSON.parse(JSON.stringify(obj)),
                event: event,
                clientX: event.clientX,
                clientY: event.clientY,
                mode: 'move'
            }
            event.stopPropagation()
            // } else {
            //     if (!obj.element.empty) {
            //         self.moveObj = {
            //             object: obj,
            //             mode: 'link',
            //             event: event,
            //             clientX: event.clientX,
            //             clientY: event.clientY
            //         }
            //     }
            // }
        })
    }
    if (element.pathInfo) {
        for (var i = 0; i < shapeNodes.length; i++) {
            var node = shapeNodes[i].node
            addEvents(node)
        }
    } else {
        var node = obj.shapeGroup.select('image').node
        addEvents(node)
    }
    var resizePoints = {}
    var pointRadius = 5
    resizePoints.topLeft = g.circle(0, 0, pointRadius)
    resizePoints.topLeft.attr({
        class: 'dfd-resize-points dfd-resize-top-left'
    })
    resizePoints.topRight = g.circle(dim.width, 0, pointRadius)
    resizePoints.topRight.attr({
        class: 'dfd-resize-points dfd-resize-top-right'
    })
    resizePoints.bottomLeft = g.circle(0, dim.height, pointRadius)
    resizePoints.bottomLeft.attr({
        class: 'dfd-resize-points dfd-resize-bottom-left'
    })
    resizePoints.bottomRight = g.circle(dim.width, dim.height, pointRadius)
    resizePoints.bottomRight.attr({
        class: 'dfd-resize-points dfd-resize-bottom-right'
    })

    resizePoints.left = g.circle(0, dim.height / 2, pointRadius)
    resizePoints.left.attr({
        class: 'dfd-resize-points dfd-resize-left'
    })

    resizePoints.top = g.circle(dim.width / 2, 0, pointRadius)
    resizePoints.top.attr({
        class: 'dfd-resize-points dfd-resize-top'
    })

    resizePoints.right = g.circle(dim.width, dim.height / 2, pointRadius)
    resizePoints.right.attr({
        class: 'dfd-resize-points dfd-resize-right'
    })
    resizePoints.bottom = g.circle(dim.width / 2, dim.height, pointRadius)
    resizePoints.bottom.attr({
        class: 'dfd-resize-points dfd-resize-bottom'
    })
    for (var p in resizePoints) {
        var pEle = resizePoints[p].node
        pEle.addEventListener('click', function() {
            event.stopPropagation()
        })
    }
    function updateMoveObject(mode, event) {
        self.moveObj = {
            object: obj,
            object_: JSON.parse(JSON.stringify(obj)),
            mode: mode,
            event: event,
            clientX: event.clientX,
            clientY: event.clientY
        }
        event.stopPropagation()
    }
    resizePoints.topLeft.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-nw', event)
    })
    resizePoints.topRight.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-ne', event)
    })
    resizePoints.bottomLeft.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-sw', event)
    })
    resizePoints.bottomRight.node.addEventListener('mousedown', function(
        event
    ) {
        updateMoveObject('resize-se', event)
    })
    resizePoints.left.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-w', event)
    })
    resizePoints.top.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-n', event)
    })
    resizePoints.right.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-e', event)
    })
    resizePoints.bottom.node.addEventListener('mousedown', function(event) {
        updateMoveObject('resize-s', event)
    })
    obj.resizePoints = resizePoints
    self.nodes[obj.id] = obj
    if (!obj.element.empty) {
        self.addLinkPoints(obj)
    }
    return obj
}
DFD.prototype.addLinkPoints = function(obj) {
    var self = this
    var dim = obj.dim
    var g = obj.g
    var linkPoints = {}
    var pointRadius = 10
    linkPoints.top = g.circle(dim.width / 2, 0, pointRadius)
    linkPoints.top.attr({
        class: 'dfd-link-points dfd-link-points-top',
        linkType: 'top'
    })
    linkPoints.left = g.circle(0, dim.height / 2, pointRadius)
    linkPoints.left.attr({
        class: 'dfd-link-points dfd-link-points-left',
        linkType: 'left'
    })
    linkPoints.right = g.circle(dim.width, dim.height / 2, pointRadius)
    linkPoints.right.attr({
        class: 'dfd-link-points dfd-link-points-right',
        linkType: 'right'
    })
    linkPoints.bottom = g.circle(dim.width / 2, dim.height, pointRadius)
    linkPoints.bottom.attr({
        class: 'dfd-link-points dfd-link-points-bottom',
        linkType: 'bottom'
    })
    function linkEvent(type, event) {
        self.moveObj = {
            object: obj,
            mode: 'link',
            linkType: type,
            event: event,
            clientX: event.clientX,
            clientY: event.clientY
        }
        self.clearSelect()
        event.stopPropagation()
    }
    forList(linkPoints, function(p) {
        p.node.addEventListener('mousemove', function(event) {
            //
            if (self.moveObj.mode != 'link') {
                event.stopPropagation()
            }
        })
    })
    function mouseUpEvent(type, event) {
        if (self.moveObj.mode == 'link') {
            var obj_ = self.createLink(
                self.nodes[self.moveObj.object.id],
                self.nodes[obj.id],
                {
                    messageText: 'Message',
                    sourceType: self.moveObj.linkType,
                    targetType: type
                }
            )

            //event.stopPropagation()
            //self.moveObj = {}
            //self.updateText(obj.messageText, obj.message)
        }
    }
    linkPoints.top.node.addEventListener('mousedown', function(event) {
        linkEvent('top', event)
    })
    linkPoints.left.node.addEventListener('mousedown', function(event) {
        linkEvent('left', event)
    })
    linkPoints.right.node.addEventListener('mousedown', function(event) {
        linkEvent('right', event)
    })
    linkPoints.bottom.node.addEventListener('mousedown', function(event) {
        linkEvent('bottom', event)
    })
    linkPoints.top.node.addEventListener('mouseup', function(event) {
        mouseUpEvent('top', event)
    })
    linkPoints.left.node.addEventListener('mouseup', function(event) {
        mouseUpEvent('left', event)
    })
    linkPoints.right.node.addEventListener('mouseup', function(event) {
        mouseUpEvent('right', event)
    })
    linkPoints.bottom.node.addEventListener('mouseup', function(event) {
        mouseUpEvent('bottom', event)
    })
    obj.linkPoints = linkPoints
}
DFD.prototype.updateResizeObj = function(obj) {
    var self = this
    var resizePoints = obj.resizePoints
    var linkPoints = obj.linkPoints
    var dim = obj.dim
    resizePoints.topRight.attr({cx: dim.width})
    resizePoints.bottomLeft.attr({cy: dim.height})
    resizePoints.bottomRight.attr({cy: dim.height, cx: dim.width})

    resizePoints.top.attr({cx: dim.width / 2})
    resizePoints.right.attr({cy: dim.height / 2, cx: dim.width})
    resizePoints.left.attr({cy: dim.height / 2})
    resizePoints.bottom.attr({cx: dim.width / 2, cy: dim.height})
    if (linkPoints) {
        linkPoints.top.attr({cx: dim.width / 2})
        linkPoints.right.attr({cy: dim.height / 2, cx: dim.width})
        linkPoints.left.attr({cy: dim.height / 2})
        linkPoints.bottom.attr({cx: dim.width / 2, cy: dim.height})
    }
    obj.message.attr({x: dim.width / 2, y: dim.height + 20})
    // if (obj.element.empty) {
    //     obj.message.attr({x: obj.dim.width / 2, y: -5})
    // }
    self.updateText(obj.messageText, obj.message)
    self.messageBox(obj)
    // var bBox = obj.message.getBBox()
    // obj.messageRect.attr({
    //     x: self.selectedObject.dim.width / 2 - bBox.width / 2,
    //     y: self.selectedObject.dim.height,
    //     width: bBox.width,
    //     height: bBox.height
    // })
    insertBefore(obj.message.node, obj.messageRect.node)
}
DFD.prototype.messageBox = function(obj) {
    var self = this
    if (obj.messageRect == undefined) {
        return
    }
    var bBox = obj.message.getBBox()
    obj.messageRect.attr({
        x: obj.dim.width / 2 - bBox.width / 2,
        y: obj.message.attr('y') - bBox.height + 2,
        width: bBox.width,
        height: bBox.height + 2
    })
}
DFD.prototype.tSpan = function(canvas, x, y, text) {
    return canvas.text(x, y, text)
}
DFD.prototype.export = function() {
    var self = this
    var exportJSON = {}
    var nodes = {}
    forList(self.nodes, function(n) {
        var node = {}
        var copy = ['id', 'dim', 'element', 'messageText']
        forList(copy, function(c) {
            node[c] = n[c]
        })
        nodes[n.id] = node
        var list = Array.prototype.slice.call(
            n.shapeGroup.node.parentNode.parentNode.childNodes
        )
        node.z = list.indexOf(n.shapeGroup.node.parentNode)
    })
    var links = []
    forList(self.links, function(l) {
        var list = Array.prototype.slice.call(
            l.message.node.parentNode.childNodes
        )
        var link = {
            messageText: l.messageText,
            sourceID: l.source.id,
            targetID: l.target.id,
            sourceType: l.sourceType,
            targetType: l.targetType,
            z: list.indexOf(l.message.node),
            element: l.element,
            linkCount: l.linkCount
        }
        links.push(link)
    })
    exportJSON.nodes = nodes
    exportJSON.links = links
    exportJSON.linkCount = self.linkCount
    return exportJSON
}
DFD.prototype.import = function(json) {
    var self = this
    var nodes = []
    forList(json.nodes, function(n) {
        nodes.push(n)
    })
    nodes.sort(function(a, b) {
        var diff = a.z - b.z
        return diff / Math.abs(diff)
    })
    json.links.sort(function(a, b) {
        var diff = a.z - b.z
        return diff / Math.abs(diff)
    })
    forList(nodes, function(n) {
        var obj = self.addNode(n.element, undefined, {
            dim: n.dim,
            id: n.id,
            messageText: n.messageText
        })
        self.updateText(obj.messageText, obj.message)
    })

    forList(json.links, function(n) {
        var obj = self.createLink(
            self.nodes[n.sourceID],
            self.nodes[n.targetID],
            {
                messageText: n.messageText,
                sourceType: n.sourceType,
                targetType: n.targetType,
                element: n.element,
                linkCount: n.linkCount
            }
        )

        //self.updateText(obj.messageText, obj.message)
    })
    self.updateLinks()
}
DFD.prototype.toPng = function(callback) {
    var self = this
    var node = self.canvas.node.parentNode
    domtoimage
        .toPng(node)
        .then(function(dataUrl) {
            callback(dataUrl)
        })
        .catch(function(error) {
            console.error('oops, something went wrong!', error)
        })
}
DFD.prototype.pdfMakeContent = function(callback) {
    var self = this
    self.toPng(function(img) {
        pdfMake.pageLayout = {
            height: 792,
            width: 612,
            margins: Array(4).fill(40)
        }
        var dd = {
            content: [
                'Heading 1',
                'Heading 2',
                {
                    image: 'dfd',
                    width: 612 - 80
                }
            ],
            images: {dfd: img}
        }
        callback(dd)
    })
}
function makePDF(imagePath, text) {
    pdfMake
        .createPdf({
            content: [
                text,
                {
                    image: 'dfd',
                    width: 612 - 80
                }
            ],
            images: {dfd: image}
        })
        .print()
}
